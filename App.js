import React from 'react';
import { ActivityIndicator,StyleSheet, Text, View ,StatusBar} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import {store ,persistor} from './app/redux/store';
import {Routes} from './app/config/routes';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';

import { AlertProvider} from './app/components/Alert';




export default class App extends React.Component {
 //loading  state
  state = {
    loaded: false,
  };


  //loading assets  when component loads
  componentWillMount() {
    this.loadAssets().then(this.onAssetsLoaded);
  }

  //fuctions that  loads assets
  loadAssets = async () => {
    await Font.loadAsync({
     
      'OpenSans-Bold': require('./app/assets/fonts/OpenSans-Bold.ttf'),
      'OpenSans-Semibold': require('./app/assets/fonts/OpenSans-Semibold.ttf'),
      'OpenSans-Regular': require('./app/assets/fonts/OpenSans-Regular.ttf'),
      'OpenSans-Italic': require('./app/assets/fonts/OpenSans-Italic.ttf'),
      
  });
};

//removing loadig screen
onAssetsLoaded = () => {
  this.setState({ loaded: true });
};



//renders loading
renderLoading = () => (<AppLoading />);
  
//renders applications
renderApplication = () => (
 
  <AlertProvider>
         <Provider store={ store }>
              <PersistGate loading={<ActivityIndicator size="large" color="#0000ff" />} persistor={persistor}>
                <Routes/> 
            </PersistGate>
         </Provider>
</AlertProvider>
   


  


);


  render() {
    return !this.state.loaded ? this.renderLoading() : this.renderApplication();
  
  }
}

