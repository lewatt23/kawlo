import React from "react";
import { TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  createDrawerNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer
} from "react-navigation";
import { Splash } from "../modules/splash";
import { Notes, Subjects, Article } from "../modules/notes";
import { Home } from "../modules/home";
import { Answers } from "../modules/home";
import { ImageView, Question, DrawerContent } from "../modules/home";
import { Questions,Question as QuestionPaper,Category } from "../modules/questions";
import { Corrections } from "../modules/corrections";
import { Profile, Password, DisplayPic, Username } from "../modules/profile";
import { Auth } from "../modules/auth";

//Creating Routes

//profile stack

const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: "Profile",
      tabBarLabel: "Profile"
    }
  },
  Password: {
    screen: Password,
    navigationOptions: {
      title: "Update Password",
      tabBarLabel: "Update Password"
    }
  },

  DisplayPic: {
    screen: DisplayPic,
    navigationOptions: {
      title: "Update Display Picture",
      tabBarLabel: "Update Display Picture"
    }
  },
  Username: {
    screen: Username,
    navigationOptions: {
      title: "Update Username",
      tabBarLabel: "Update Username"
    }
  }
});

//home stack

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        title: "Home",
        tabBarLabel: "Home",

        tabBarIcon: ({ tintColor }) => <Icon name="pencil" size={24} />
      }
    },
    Question: {
      screen: Question,
      navigationOptions: {
        title: "Ask question"
      }
    },

    Auth: {
      screen: Auth,
      navigationOptions: {
        title: "Auth",
        header: null
      }
    },
    Splash: {
      //splash screen
      screen: Splash,
      navigationOptions: {
        header: null
      }
    },

    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        title: "Profile setting",
        header: null
      }
    },

    Answers: {
      screen: Answers,
      navigationOptions: {
        title: "Answers"
      }
    },
    ImageView: {
      screen: ImageView,
      navigationOptions: {
        title: "Image"
      }
    }
  },
  {
    transparentCard: true,
    cardStyle: { opacity: 1 }
  }
);

//NoteStack
const NoteStack = createStackNavigator({
  NotesList: {
    screen: Subjects,
    navigationOptions: {
      title: "NotesList"
    }
  },

  Notes: {
    screen: Notes,
    navigationOptions: {
      title: "Notes",
      tabBarLabel: "Notes",

      tabBarIcon: ({ tintColor }) => <Icon name="pencil" size={24} />
    }
  },

  Article: {
    screen: Article,
    navigationOptions: {
      title: "Article"
    }
  }
});

//questions stack
const QuestionsStack = createStackNavigator({
  NotesList: {
    screen: Questions,
    navigationOptions: {
      title: "Questions"
    }
  },
  QuestionPaper: {
    screen: QuestionPaper,
    navigationOptions: {
      title: "QuestionPaper"
    }
  },
  Category: {
    screen: Category,
    navigationOptions: {
      title: "Category"
    }
  }





});
//corrections stack
const CorrectionsStack = createStackNavigator({
  NotesList: {
    screen: Corrections,
    navigationOptions: {
      title: "Corrections"
    }
  }
});

//option to not dispay tab on Auth page
HomeStack.navigationOptions = ({ navigation }) => {
  const currentScreenPath = navigation.router.getPathAndParamsForState(
    navigation.state
  ).path;
  const Auth = currentScreenPath === "Auth" || currentScreenPath === "Splash";
  return {
    tabBarVisible: Auth === false
  };
};

//tab bottom menu
const AppButtonNav = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: "Answers",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="pencil" color={tintColor} size={24} />
        )
      }
    },
    Questions: {
      screen: QuestionsStack,
      navigationOptions: {
        tabBarLabel: "Exam questions",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="folder" color={tintColor} size={24} />
        )
      }
    },
 

    Notes: {
      screen: NoteStack,
      navigationOptions: {
        tabBarLabel: "Notes",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="notebook" color={tintColor} size={24} />
        )
      }
    },

    Correction: {
      screen: CorrectionsStack,
      navigationOptions: {
        tabBarLabel: "Corrections",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="file-document-box-multiple" color={tintColor} size={24} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "#75a5ee", // active icon color
      inactiveTintColor: "#586589", // inactive icon color
      style: {
        backgroundColor: "#fff" // TabBar background
      }
    }
  }
);

//tab nav creator
const TabScreens = createStackNavigator(
  {
    Tabs: {
      screen: AppButtonNav,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
            <Icon name="menu" size={30} />
          </TouchableOpacity>
        )
      })
    }
  },
  { headerMode: "none" }
);

//this create a left drawer menu
const AppdrawNav = createDrawerNavigator(
  {
    Tabs: {
      screen: TabScreens,
      navigationOptions: {
        drawerLabel: "Home",
        drawerIcon: ({ tintColor }) => (
          <Icon name="home" color="#5d95d9" size={20} />
        )
      }
    }
  },
  {
    contentComponent: props => {
      return <DrawerContent {...props} />;
    }
  }
);

const AppContainer = createAppContainer(AppdrawNav);

export class Routes extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <AppContainer />;
  }
}

export default Routes;
