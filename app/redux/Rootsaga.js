import {fork,all} from 'redux-saga/effects';
import {Login,Register,Page,Notification} from '../modules/auth/actions';
import {AddQuestion,GetQuestion,AddReply,GetReply} from '../modules/home/actions';
import {watcherGetnotes} from '../modules/notes/actions';
import {watcherUpdatePassword,watcherUpdateUsername,watcherUpdatePhoto} from '../modules/profile/actions';
import { networkSaga } from 'react-native-offline';


// all  sagas  are  found here
export default function* rootSaga () {

  yield all([
    fork(AddQuestion),
    fork(GetQuestion),
    fork(GetReply),
    fork(AddReply),
    fork(Login),
    fork(Page),
    fork(Register),
    fork(watcherGetnotes),
    fork(watcherUpdatePassword),
    fork(watcherUpdateUsername),
    fork(watcherUpdatePhoto),
    fork(Notification),
    fork(networkSaga, { pingInterval: 200, pingInBackground: true }),
   
  ])

}