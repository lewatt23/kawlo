import { applyMiddleware, compose, createStore} from 'redux';
import  rootSaga   from "./Rootsaga";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import createSagaMiddleware from "redux-saga";
import rootReducer from './rootReducer'; //Import the root reducer
import {
  withNetworkConnectivity,
  reducer as network,
  createNetworkMiddleware
} from "react-native-offline";

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
const networkMiddleware = createNetworkMiddleware({
  queueReleaseThrottle: 200,
});

//redux poersist
const persistConfig = {
  key: 'root',
  storage,
  blacklist: ["isNetworkBannerVisible"] 
}
const persistedReducer = persistReducer(persistConfig, rootReducer)


export  const store = createStore(persistedReducer,compose(applyMiddleware(networkMiddleware, sagaMiddleware)));
export  const persistor = persistStore(store);
  

// run the saga
sagaMiddleware.run(rootSaga);
