
import { combineReducers } from 'redux';

import {authReducer } from "../modules/auth";
import {questionReducer } from "../modules/home";
import {notesReducer } from "../modules/notes";
import alertReducer from './../components/Alert/reducer';
import {reducer as network} from "react-native-offline";
// Combine all the reducers

const rootReducer = combineReducers({ 
  
  authReducer:authReducer ,
  questionReducer:questionReducer,
  notesReducer:notesReducer,
  alertReducer:alertReducer,
  network,


});

export default rootReducer;
