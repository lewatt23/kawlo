import moment from 'moment';




export const  delay = (ms) => new Promise(res => setTimeout(res, ms));

export const truncate = input =>
  input.length > 200 ? `${input.substring(0, 200)}...` : input;


export const timeFromNow=(timestamp)=>{
    
    return moment(timestamp).fromNow();
}