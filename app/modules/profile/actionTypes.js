
export const CHANGE_PASSWORD_START = '[Settings] CHANGE_PASSWORD_START ';
export const CHANGE_PASSWORD_FAIL = '[Settings] CHANGE_PASSWORD_FAIL';
export const CHANGE_PASSWORD_SUCCESS  = '[Settings] CHANGE_PASSWORD_SUCCESS';

export const CHANGE_USERNAME_START = '[Settings] CHANGE_USERNAME_START ';
export const CHANGE_USERNAME_FAIL = '[Settings] CHANGE_USERNAME_FAIL';
export const CHANGE_USERNAME_SUCCESS  = '[Settings] CHANGE_USERNAME_SUCCESS';


export const CHANGE_DISPLAY_PICTURE_START = '[Settings] CHANGE_DISPLAY_PICTURE_START ';
export const CHANGE_DISPLAY_PICTURE_FAIL = '[Settings] CHANGE_DISPLAY_PICTURE_FAIL';
export const CHANGE_DISPLAY_PICTURE_SUCCESS  = '[Settings] CHANGE_DISPLAY_PICTURE_SUCCESS';

