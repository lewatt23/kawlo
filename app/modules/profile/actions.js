import * as t from './actionTypes';
import * as api from './api';
import { takeLatest, put, call,select } from 'redux-saga/effects';
import { LOGGED_IN } from './../auth/actionTypes';

export const getAuthState = (state) => state.authReducer.user;


export  const updatePassword =(data) => {
     
  return {
      type:t.CHANGE_PASSWORD_START,
      data:data
  };


}
export  const updateUsername =(data) => {
     
  return {
      type:t.CHANGE_USERNAME_START,
      data:data
  };


}
export  const updatePhoto =(data) => {
     
  return {
      type:t.CHANGE_DISPLAY_PICTURE_START,
      data:data
  };


}




//funtion to change users password
function* updatePasswordFireBase(action) {
  try {
      const user = yield call(api.changePassword,action.data);
         
         } catch (error) {
         console.log('saga fail: ', error);
        
         
     }
 }

 //function  to update username
function* updateUsernameFireBase(action) {
  try {
    
    
   

         const user = yield call(api.changeUsername,action.data);

         if(user){

          const {username} = action.data
        
          let authUser = yield select(getAuthState);
   
          

          authUser.user.displayName = username;
    
         
  
        yield put({ type:LOGGED_IN, payload: {user:authUser} });
  
      }
            
     } catch (error) {
         console.log('saga fail:', error);
        
         
     }
 }
 //function  to update photo
function* updatePhotoFireBase(action) {
  try {
    
    
   

         const user = yield call(api.changePhoto,action.data);
 
    if(user.state){
      
       
        let authUser = yield select(getAuthState);
      
        authUser.user.photoURL = user.image;
      

      yield put({ type:LOGGED_IN, payload: {user:authUser} });

    }

            
     } catch (error) {
         console.log('saga fail: ', error);
        
         
     }
 }




 export function* watcherUpdatePassword(action) {
  yield takeLatest(t.CHANGE_PASSWORD_START,updatePasswordFireBase);
}
 export function* watcherUpdateUsername(action) {
  yield takeLatest(t.CHANGE_USERNAME_START,updateUsernameFireBase);
}
 export function* watcherUpdatePhoto(action) {
  yield takeLatest(t.CHANGE_DISPLAY_PICTURE_START,updatePhotoFireBase);
}