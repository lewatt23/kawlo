import firebase,{ auth, database, firestore } from "../../config/firebase";




// function to  reauthenticate users
function reauthenticate(currentPassword) {
  const user = firebase.auth().currentUser;

  console.log("user-email",user)

  const cred = firebase.auth.EmailAuthProvider.credential(
      user.email, currentPassword);
  return user.reauthenticateWithCredential(cred);
}


// function to  update user password
export function changePassword(data){

  const {oldpassword,newpassword} = data

  return reauthenticate(oldpassword).then(() => {
    const user = firebase.auth().currentUser;
    user.updatePassword(newpassword).then(() => {
      return true;
    }).catch((error) => { return console.log(error); });
  }).catch((error) => {  return console.log(error); });
}




// function  to change user email
export function changeEmail(currentPassword, newEmail){
  return  reauthenticate(currentPassword).then(() => {
    const user = firebase.auth().currentUser;
    user.updateEmail(newEmail).then(() => {
      console.log("Email updated!");
    }).catch((error) => { console.log(error); });
  }).catch((error) => { console.log(error); });
}



//function to change username

export function changeUsername(data){
 
   const {username} = data;
   


   return  firebase.auth().currentUser.updateProfile({
                displayName: username
              })
              .then(() => {
               
                return true;
                
              })
              .catch(err => {
                return  false;
        });


}



export async  function changePhoto(data){

  const {user,uri} = data; 


  let image ="";

  if(uri !== null){
    // console.log(uri);
   image = await  uploadAvater(uri,user.user.uid);
 // console.log(image);
  }




  await database.ref('users')
  .child(user.user.uid)
  .update({avater:image})
  .then(()=>{
     console.log("users avater updated");
  })
  .catch(err=>{
    console.log(err);
  });
  

  
  return  firebase.auth().currentUser.updateProfile({
    photoURL: image
  })
  .then(() => {
    return {state:true,image:image}
    
    
  })
  .catch(err => {
    return false;
});



//updating users database  



}







export const uploadAvater = async (uri,userId) =>{
  // Why are we using XMLHttpRequest? See:
// https://github.com/expo/expo/issues/2402#issuecomment-443726662

const blob = await new Promise((resolve, reject) => {
 const xhr = new XMLHttpRequest();
 xhr.onload = function() {
   resolve(xhr.response);
 };
 xhr.onerror = function(e) {
   
   reject(new TypeError('Network request failed'));
 };
 xhr.responseType = 'blob';
 xhr.open('GET', uri, true);
 xhr.send(null);
 });

 const ref = firebase.storage().ref().child(`avaters/user-${userId}`);
 const snapshot = await ref.put(blob);
  // We're done with the blob, close and release it
  blob.close();
 
 return await snapshot.ref.getDownloadURL();
}