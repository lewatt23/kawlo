import * as t from './actionTypes';

let initialState = { isLoggedIn: false, user: null, isOnline: false, lastSeen: null };

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.LOGGED_OUT:
      state = Object.assign({}, state, initialState);
      return state;

    case t.LOGGED_IN:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.SET_ONLINE:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.SET_OFFLINE:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    default:
      return state;
  }
};

export default authReducer;
