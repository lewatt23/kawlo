import React from 'react';
import { 
  StyleSheet,
  Text,
  TextInput,
  View ,
  TouchableOpacity,
  ActivityIndicator,
  Alert

}
from 'react-native';
import {connect} from 'react-redux';
import * as actions from  '../actions';
import  {UtilStyles,GradientButton} from '../../../components'
import { withNavigation } from 'react-navigation';








 class Username extends React.Component {
  

   state = {
     fields:{
 
      username:'',
    
     },
      errors:{ },
      message:'',
     
   }
   

   onSignIn = () =>{
    if (this.validateForm()) {
     
       if (this.props.user !== null) {
        let data ={
          "username":this.state.fields.username,
          
        }
        
       
        let fields = {};
        fields["username"] ="";
        
         
         this.setState({fields:fields});
         this.props.updateUsername(data);
        }else{
          this.checkAuth();
        }
       
    }
  }



 //function use to validate form
  validateForm= ()=>{
    
    let fields= this.state.fields;
     let errors = {};
     let formIsValid = true;

     
    if (!fields["username"]) {
        formIsValid = false;
        errors["username"] = "*Please your new username.";
      }
  
       
    
     
      this.setState({
        errors: errors
      });
      return formIsValid;

 };



 checkAuth=()=>{
  
  Alert.alert(
    'login Erro',
    'Sorry you have to  login to continue!',
    [
      {text: 'Login', onPress: () =>  this.props.navigation.navigate('Auth')},
      {
        text: 'Register',
        onPress: () => this.props.navigation.navigate('Auth'),
        style: 'cancel',
      },
      {text: 'cancel', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );

}


  
  render() {

   
    
    


    return (
      
        <View style={ styles.container }>
        
   
                   
                         <TextInput
                          style={UtilStyles.textInput}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          editable = {true}
                          placeholder='Enter new username' 
                          placeholderTextColor='#757575'
                          value={this.state.fields.username}
                         
                         onChangeText={username =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                username: username
                            }
                         }))} 
                         autoFocus={false}
                        />
                        <Text style={UtilStyles.validate}>{this.state.errors.username}</Text> 
                      


                        <View style={{width:300}}>
                          <TouchableOpacity
                          onPress={this.onSignIn}
                          >
                            <GradientButton
                                  text='Update Username'
                            />
                          </TouchableOpacity>
                        </View>
                        <Text style={UtilStyles.validate}>{this.props.message}</Text> 
                        
                       
                      
                    
            </View>
       

    );
    }

  


  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  }
});

const mapDispatchToProps = dispatch => ({
  updateUsername: (data) => dispatch(actions.updateUsername(data)),

 }); 
 const mapStateToProps = state => {
  return {
    user:state.authReducer.user,
    notif:state.authReducer,
    
  }
};

export default  withNavigation(connect(mapStateToProps,mapDispatchToProps)(Username));