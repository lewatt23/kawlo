import React from 'react';
import { 
  StyleSheet,
  Text,
  TextInput,
  View ,
  TouchableOpacity,
  ActivityIndicator,
  Alert

}
from 'react-native';
import {connect} from 'react-redux';
import * as actions from  '../actions';
import  {UtilStyles,GradientButton} from '../../../components'
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';







class DisplayPic extends React.Component {
 

  state = {
    fields:{
     uri:'',
    
    },
     errors:{ },
     message:'',
    
  }
  

  //method to sigin using
 onSignIn = () =>{
   if (this.validateForm()) {
    
      if (this.props.user !== null) {
       let data ={
         "uri":this.state.fields.uri,
         "user":this.props.user
         
       }
       console.log(this.state.fields.uri);
      
       let fields = {};
       fields["uri"] ="";
       
        this.setState({fields:fields});
       this.props.updatePhoto(data);
       }else{
         this.checkAuth();
       }
      
   }
 }


//image picker
 pickImage = async () => {
   await Permissions.askAsync(Permissions.CAMERA_ROLL);
   await Permissions.askAsync(Permissions.CAMERA);


   let result = await ImagePicker.launchImageLibraryAsync({
     allowsEditing: true,
     aspect: [3, 2],
    
    });


   

   if(!result.cancelled){
     this.setState(prevState =>
       ({
         fields: {
             ...prevState.uri,
             uri: result.uri
         }
      }))

    
     
   }



 }


//function use to validate form
 validateForm= ()=>{
   
   let fields= this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["uri"]) {
     formIsValid = false;
     errors["uri"] = "*Please select  your picture.";
   }

      
   
    
     this.setState({
       errors: errors
     });
     return formIsValid;

};



checkAuth=()=>{
 
 Alert.alert(
   'login Erro',
   'Sorry you have to  login to continue!',
   [
     {text: 'Login', onPress: () =>  this.props.navigation.navigate('Auth')},
     {
       text: 'Register',
       onPress: () => this.props.navigation.navigate('Auth'),
       style: 'cancel',
     },
     {text: 'cancel', onPress: () => console.log('OK Pressed')},
   ],
   {cancelable: false},
 );

}


 
 render() {

  
   


   return (
     
       <View style={ styles.container }>
                   <Text >Select your display picture</Text> 
       
                              <TouchableOpacity
                                     onPress={this.pickImage}
                             >
                             <Icon style={UtilStyles.feeds} name="camera" size={50} color="#5d95d9"/>
                             </TouchableOpacity> 
                       <Text style={UtilStyles.validate}>{this.state.errors.repassword}</Text> 
                  
                       <View style={{width:300}}>
                         <TouchableOpacity
                         onPress={this.onSignIn}
                         >
                           <GradientButton
                                 text='Update picture'
                           />
                         </TouchableOpacity>
                       </View>
                       <Text style={UtilStyles.validate}>{this.props.message}</Text> 
                       
                      
                     
                   
           </View>
      

   );
   }

 


 
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  }
});

const mapDispatchToProps = dispatch => ({
  updatePhoto:(data) => dispatch(actions.updatePhoto(data)),

 });
const mapStateToProps = state => {
 return {
   user:state.authReducer.user,
   notif:state.authReducer,
   
 }
};

export default  withNavigation(connect(mapStateToProps,mapDispatchToProps)(DisplayPic));