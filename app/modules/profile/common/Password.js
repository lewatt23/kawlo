import React from 'react';
import { 
  StyleSheet,
  Text,
  TextInput,
  View ,
  TouchableOpacity,
  ActivityIndicator,
  Alert

}
from 'react-native';
import {connect} from 'react-redux';
import * as actions from  '../actions';
import  {UtilStyles,GradientButton} from '../../../components'
import { withNavigation } from 'react-navigation';








 class Password extends React.Component {
  

   state = {
     fields:{
      oldpassword:'',
      newpassword:'',
      repassword:'',
     },
      errors:{ },
      message:'',
     
   }
   


   componentDidMount(){
   }

   //method to sigin using
  onSignIn = () =>{
    if (this.validateForm()) {
     
       if (this.props.user !== null) {
        let data ={
          "oldpassword":this.state.fields.oldpassword,
          "newpassword":this.state.fields.newpassword
        }
        
       
        let fields = {};
        fields["oldpassword"] ="";
        fields["newpassword"] ="";
         fields["repassword"] = "";
         
         this.setState({fields:fields});
         this.props.updatePassword(data);
        }else{
          this.checkAuth();
        }
       
    }
  }



 //function use to validate form
  validateForm= ()=>{
    
    let fields= this.state.fields;
     let errors = {};
     let formIsValid = true;

     if (!fields["oldpassword"]) {
      formIsValid = false;
      errors["oldpassword"] = "*Please enter your Old Password.";
    }

    if (!fields["newpassword"]) {
        formIsValid = false;
        errors["newpassword"] = "*Please enter your New password.";
      }
    if (!fields["repassword"]) {
        formIsValid = false;
        errors["repassword"] = "*Please Confirm your password.";
      }
    if (fields["repassword"] !== fields["newpassword"] ) {
        formIsValid = false;
        errors["repassword"] = "*Passwords not the same!.";
      }
    
       
    
     
      this.setState({
        errors: errors
      });
      return formIsValid;

 };



 checkAuth=()=>{
  
  Alert.alert(
    'login Erro',
    'Sorry you have to  login to continue!',
    [
      {text: 'Login', onPress: () =>  this.props.navigation.navigate('Auth')},
      {
        text: 'Register',
        onPress: () => this.props.navigation.navigate('Auth'),
        style: 'cancel',
      },
      {text: 'cancel', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );

}


  
  render() {


    return (
      
        <View style={ styles.container }>
        
   
                        <TextInput
                        style={UtilStyles.textInput}
                        underlineColorAndroid="transparent"
                        editable = {true}
                        autoCorrect={false}
                        placeholder='Old password'
                        placeholderTextColor='#757575'
                        value={this.state.fields.oldpassword}
                        secureTextEntry={true}
                        onChangeText={oldpassword =>
                          this.setState(prevState =>
                           ({
                             fields: {
                                 ...prevState.fields,
                                 oldpassword: oldpassword
                             }
                          }))}
                        autoFocus={false}
                       />
                       <Text style={UtilStyles.validate}>{this.state.errors.oldpassword}</Text> 


                         <TextInput
                          style={UtilStyles.textInput}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          editable = {true}
                          placeholder='New Password' 
                          placeholderTextColor='#757575'
                          value={this.state.fields.newpassword}
                         secureTextEntry={true}
                         onChangeText={newpassword =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                newpassword: newpassword
                            }
                         }))} 
                         autoFocus={false}
                        />
                        <Text style={UtilStyles.validate}>{this.state.errors.newpassword}</Text> 

                         <TextInput
                          style={UtilStyles.textInput}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          editable = {true}
                          placeholder='Retype New Password' 
                          placeholderTextColor='#757575'
                          value={this.state.fields.repassword}
                         secureTextEntry={true}
                         onChangeText={repassword =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                repassword: repassword
                            }
                         }))} 
                         autoFocus={false}
                        />
                        <Text style={UtilStyles.validate}>{this.state.errors.repassword}</Text> 
                   
                        <View style={{width:300}}>
                          <TouchableOpacity
                          onPress={this.onSignIn}
                          >
                            <GradientButton
                                  text='Update password'
                            />
                          </TouchableOpacity>
                        </View>
                        <Text style={UtilStyles.validate}>{this.props.message}</Text> 
                        
                       
                      
                    
            </View>
       

    );
    }

  


  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  }
});

const mapDispatchToProps = dispatch => ({
  updatePassword: (data) => dispatch(actions.updatePassword(data)),

 }); 
 const mapStateToProps = state => {
  return {
    user:state.authReducer.user,
    notif:state.authReducer,
    
  }
};

export default  withNavigation(connect(mapStateToProps,mapDispatchToProps)(Password));