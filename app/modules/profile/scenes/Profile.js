import React from 'react';
import {Image, StyleSheet,Modal,Button, Text, View , TouchableOpacity,TouchableHighlight,FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {UtilStyles } from '../../../components';
import { ScrollView } from 'react-native-gesture-handler';
import {connect} from 'react-redux';


class Profile extends React.Component {
 
  static navigationOptions = ({ navigation }) => ({
    
    headerLeft: (
        <TouchableOpacity
            style={{marginHorizontal: 20,}}
            onPress={() => navigation.openDrawer()}>
            <Icon name="menu" color={'#75a5ee'} size={20} />
            
        </TouchableOpacity>
    ),
});



  render() {

   const {user} = this.props;

    return (
     
      <View style={UtilStyles.container}>


   

              <View style={UtilStyles.header__setting}></View>
              <Image style={UtilStyles.avatar__setting} source={{uri:!!user? user.user.photoURL:'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
              <Text style={UtilStyles.name__setting} > Hello ! {!!user ?user.user.displayName:"user"}   </Text>
     
              <ScrollView>
              <View >
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate("Password",{
                
                })}
              style={UtilStyles.feedsBottom}>
                <View>
                    
                  </View>
                  
                    <View style={UtilStyles.spaceVertical}>
                      <Text style={UtilStyles.headingtitle}>
                        Password
                        </Text> 
                  </View>
                  <View>
                      <Text>
                          <Icon name="chevron-right" color={'#000'} size={40} />
                        </Text> 
                  </View>
                    

              </TouchableOpacity>
              <View style={UtilStyles.line}></View>
              </View>
              
              <View>
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate("Username",{
                
                })}
              style={UtilStyles.feedsBottom}>
                <View>
                    
                  </View>
                  
                    <View style={UtilStyles.spaceVertical}>
                      <Text style={UtilStyles.headingtitle}>
                        Username
                        </Text> 
                  </View>
                  <View>
                      <Text>
                          <Icon name="chevron-right" color={'#000'} size={40} />
                        </Text> 
                  </View>
                    

              </TouchableOpacity>
              <View style={UtilStyles.line}></View>
              </View>



              <View>
            <TouchableOpacity 
                onPress={() => this.props.navigation.navigate("DisplayPic",{
                
                })}
              style={UtilStyles.feedsBottom}>
                <View>
                    
                  </View>
                  
                    <View style={UtilStyles.spaceVertical}>
                      <Text style={UtilStyles.headingtitle}>
                        Profile picture
                        </Text> 
                  </View>
                  <View>
                      <Text>
                          <Icon name="chevron-right" color={'#000'} size={40} />
                        </Text> 
                  </View>
                    

              </TouchableOpacity>
              <View style={UtilStyles.line}></View>
              </View>



      </ScrollView>
      </View> 
      

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: "700",
    fontSize: 30,
  },
  info: {
    fontSize: 8,
    marginTop: 12,
  }
});



 const mapStateToProps = state => {
  return {
    user:state.authReducer.user,

  }
};

export default  connect(mapStateToProps)(Profile);