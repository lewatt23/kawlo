export const LOGGED_START = '[Auth] LOGGED_START';
export const LOGGED_IN = '[Auth] LOGGED_IN';
export const LOGGED_OUT = '[Auth] LOGGED_OUT';
export const LOGGED_LOADING = '[Auth] LOGGED_LOADING';


export const AuthPage = '[Auth] AuthPage';
export const CREATE_USER = '[Auth] Create User';
export const CREATE_USER_SUCCESS = '[Auth] Create User SUCCESS';
export const CREATE_USER_FAILED = '[Auth] Create User FALIED';

export const NOTIF_START = "NOTIF_START";
export const NOTIF_END = "NOTIF_END";
