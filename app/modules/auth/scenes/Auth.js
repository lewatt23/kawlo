import React from 'react';
import Login from '../common/login';
import Register from '../common/register';
import {connect} from 'react-redux';



 class Auth extends React.Component {
  

  

  render() {

  if(this.props.page === 'Register'){
    return (
      <Register/>
       );
  }else{
    return(
    <Login/>
    );
  }
   

  }
}

const mapStateToProps = state => {
  return {
    page:state.authReducer.page,
    
  }
};



export default connect(mapStateToProps)(Auth);