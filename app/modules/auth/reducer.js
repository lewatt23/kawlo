import * as t from './actionTypes';

let initialState = { isLoggedIn: false, user: null ,loading:false,message:null,page:"Login",messageReg:null,
notif:{state:false,message:null,title:null,type:null} };

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.LOGGED_OUT:
      state = Object.assign({}, state, initialState);
      return state;

    case t.LOGGED_LOADING:
    state = Object.assign({}, state, { ...action.payload });
      return state;
    case t.LOGGED_START:
    state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.AuthPage:
    state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.LOGGED_IN:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.CREATE_USER :
      state = Object.assign({}, state, { ...action.payload });
      return state;
    
    case t.CREATE_USER_SUCCESS :
      state = Object.assign({}, state, { ...action.payload });
      return state;
    
    case t.CREATE_USER_FAILED:
      state = Object.assign({}, state, { ...action.payload });
      return state;
    

    case t.NOTIF_END :
      return {
        ...state,
        ...action.payload
      
      }
    
      

    default:
      return state;
  }
};

export default authReducer;
