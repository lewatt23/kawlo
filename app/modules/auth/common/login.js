import React from 'react';
import {ImageBackground}
from 'react-native';
import {connect} from 'react-redux';
import * as actions from  '../actions';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import Button from '../../../components/atoms/buttons/button';
import Container from '../../../components/atoms/container/container';
import Input from '../../../components/atoms/inputs/input';
import connectAlert from './../../../components/Alert/connectAlert';
import Title, { TitleSub,BlodLink} from '../../../components/atoms/typography/typography';
import Loader from './../../../components/atoms/loader/loader';





/**
 * Login  screens used to login user.
 * @param {username} users name.
 * @param {password} users password
 * @return {view} home screens.
 */
 class Login extends React.Component {
  

   state = {
     fields:{
      email:'',
      password:'',
     },
      errors:{ },
      message:'',
     
   }

   //method to sigin using
  onSignIn = () =>{
  
    if (this.validateForm()) {
      let fields = {};
      fields['password'] ='';
       fields['email'] = '';
        const option = {
          ...this.state.fields
        };
        
       this.props.login(option);
        this.setState({fields:fields});
        
       
    }
  }


  //changes page
  onPagechange=()=>{
   
    const data = {
      page:'Register'
    };

    this.props.page(data)
  }

 //function use to validate form
  validateForm= ()=>{
    
    let fields= this.state.fields;
     let errors = {};
     let formIsValid = true;

     if (!fields['email']) {
      formIsValid = false;
      errors['email'] = '*Please enter your email-ID.';
    }

      if (!fields['password']) {
        formIsValid = false;
        errors['password'] = '*Please enter your password.';
      }


      if (typeof fields['email'] !== 'undefined') {
        //regular expression for email validation
        var pattern = new RegExp(/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (!pattern.test(fields['email'])) {
          formIsValid = false;
          errors['email'] = '*Please enter valid email,e.g watt23@gmail.com';
        }
      }
       
    
     
      this.setState({
        errors: errors
      });
      return formIsValid;

 };




  
  render() {
  

  
    if(this.props.message === null && this.props.user){
      this.props.navigation.navigate('Home');
    }
    if(!this.props.connection){
      this.props.alertWithType('info', ':(', 'No connection.');

    }
    
   if(this.props.loading && !this.props.user){
    return(
     <Loader
     loaderColor="#598BFF"
     loaderSize="large"
     
     />
    )
  }

    return (
 
      <ImageBackground source={require('../../../../assets/background-login.png')} style={{width: '100%', height: '100%'}}>
        
        <Container backgroundColor="transparent">

                <Input
                   underlineColorAndroid='transparent'
                   editable = {true}
                   autoCorrect={false}
                   placeholder='Email'
                   placeholderTextColor='#aeaaaa'
                   textColor='#000'
                   backgroundColor='#f9f9f9'
                   borderColor='#d2d2d2'
                   value={this.state.fields.email}
                   onChangeText={email =>
                     this.setState(prevState =>
                      ({
                        fields: {
                            ...prevState.fields,
                            email: email
                        }
                     }))}
                   autoFocus={false}
                
                
                />
                <TitleSub
                         text={this.state.errors.email}
                         textColor="red"
                         />
                       
                        <Input
                           underlineColorAndroid='transparent'
                           editable = {true}
                           autoCorrect={false}
                           placeholder='Password'
                           placeholderTextColor='#aeaaaa'
                           textColor='#000'
                           backgroundColor='#f9f9f9'
                           borderColor='#d2d2d2'
                           value={this.state.fields.password}
                           secureTextEntry={true}
                           onChangeText={password =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                password: password
                            }
                         }))} 
                         autoFocus={false}
                        />
                        <BlodLink 
                         text="Password forgot?"
                         textColor="#707070"
                         bold={false}
                         padding={'7px'}

                        
                        />
                        <TitleSub
                         text={this.state.errors.password}
                         textColor="red"
                         />
                        
                   

                        <Button buttonAction={this.onSignIn}  buttonBackgroundColor="#598bff" buttonText="Login" buttonTextColor="#fff"   />
                       
                        <BlodLink 
                         text="If you don't have account, SignUp!"
                         textColor="#598BFF"
                         boldAction={this.onPagechange}
                         bold={false}
                         padding={'7px'}

                        
                        />
                        <TitleSub
                         text={this.props.message}
                         textColor="red"
                         />
     
                    
            </Container>
     
     
     </ImageBackground>
          

     
    );
    }

  


  
}

const mapDispatchToProps = dispatch => ({
  login: (data) => dispatch(actions.login(data)),
  page:(payload)=>dispatch(actions.page(payload))
 }); 
 const mapStateToProps = state => {
  return {
    user:state.authReducer.user,
    errors:state.authReducer.errors,
    loading:state.authReducer.loading,
    message:state.authReducer.message,
    connection:state.network.isConnected
  }
};

export default  withNavigation(connect(mapStateToProps,mapDispatchToProps)(connectAlert(Login)));

Login.propTypes = {
  user: PropTypes.object,
  errors: PropTypes.string,
  loading: PropTypes.bool,
  message: PropTypes.string,
  connection: PropTypes.bool
}  