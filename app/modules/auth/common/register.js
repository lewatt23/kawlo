import React from 'react';
import {ImageBackground}
from 'react-native';
import {connect} from 'react-redux';
import * as actions from  '../actions';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import Button from '../../../components/atoms/buttons/button';
import Container from '../../../components/atoms/container/container';
import Input from '../../../components/atoms/inputs/input';
import connectAlert from './../../../components/Alert/connectAlert';
import Title, { TitleSub,BlodLink} from '../../../components/atoms/typography/typography';
import Loader from './../../../components/atoms/loader/loader';

 //@correct error not showing in the frontend during the testing phase


/**
 * Regsiter screens used to register and login user.
 * @param {username} users name.
 * @param {email} email.
 * @param {password} password
 * @param {repassword} repassword
 * @return {view} home screens.
 */
 class Register extends React.Component {
  

   state = {
     fields:{
      email:'',
      password:'',
      repassword:'',
      username:''
     },
      errors:{   }
     
   }

   //method to sigin using
  onRegister = () =>{
    if(this.validateForm()) {
      let fields = {};
      fields["password"] ="";
      fields["repassword"] ="";
       fields["email"] = "";
       fields["username"] = "";
        const option = {
          ...this.state.fields
        };
        
       this.props.register(option);
        this.setState({fields:fields});
        
       
    }
  }

  //changes page
  onPagechange=()=>{
   
    const data = {
      page:"Login"
    };

    this.props.page(data)
  }


 //function use to validate form
  validateForm= ()=>{
    
    let fields= this.state.fields;
     let errors = {};
     let formIsValid = true;
     
    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "*Please enter your password.";
    }
    if (!fields["password"].length > 8) {
      formIsValid = false;
      errors["password"] = "*Password must be greater than 8 letters.";
    }
     

     if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "*Please enter your email-ID.";
    }

     if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = "*Please enter your user name.";
    }

    if (typeof fields['email'] !== 'undefined') {
      //regular expression for email validation
      var pattern = new RegExp(/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields['email'])) {
        formIsValid = false;
        errors['email'] = '*Please enter valid email,e.g watt23@gmail.com';
      }
    }
    if (fields["password"] !== fields["repassword"] ) {
      formIsValid = false;
      errors["repassword"] = "*Password must be the same.";
    }
         

    this.setState({
        errors: errors
      });
      return formIsValid;

 };




  
  render() {
 
   if(this.props.loading && !this.props.user){
     return(
      <Loader
      loaderColor="#598BFF"
      loaderSize="large"
      
      />
     )
   }
   if(!this.props.connection){
    this.props.alertWithType('info', ':(', 'No Internet connection.');

  }

   if(this.props.message === null && this.props.user){
    this.props.navigation.navigate('Home');
  }
    return (
      <ImageBackground source={require('../../../../assets/background-register.png')} style={{width: '100%', height: '100%'}}>

                 <Container backgroundColor="transparent">

                 <Input
                   underlineColorAndroid='transparent'
                   editable = {true}
                   autoCorrect={false}
                   placeholder='Username'
                   placeholderTextColor='#aeaaaa'
                   textColor='#000'
                   backgroundColor='#f9f9f9'
                   borderColor='#d2d2d2'
                   value={this.state.fields.username}
                        onChangeText={username =>
                          this.setState(prevState =>
                           ({
                             fields: {
                                 ...prevState.fields,
                                 username: username
                             }
                          }))}
                   autoFocus={false}
                
                
                />
                  <TitleSub
                         text={this.state.errors.username}
                         textColor="red"
                         />

<Input
                   underlineColorAndroid='transparent'
                   editable = {true}
                   autoCorrect={false}
                   placeholder='Email'
                   placeholderTextColor='#aeaaaa'
                   textColor='#000'
                   backgroundColor='#f9f9f9'
                   borderColor='#d2d2d2'
                   value={this.state.fields.email}
                   onChangeText={email =>
                     this.setState(prevState =>
                      ({
                        fields: {
                            ...prevState.fields,
                            email: email
                        }
                     }))}
                   autoFocus={false}
                
                
                />
                <TitleSub
                         text={this.state.errors.email}
                         textColor="red"
                         />
                       
         
                     

                       <Input
                           underlineColorAndroid='transparent'
                           editable = {true}
                           autoCorrect={false}
                           placeholder='Password'
                           placeholderTextColor='#aeaaaa'
                           textColor='#000'
                           backgroundColor='#f9f9f9'
                           borderColor='#d2d2d2'
                           value={this.state.fields.password}
                           secureTextEntry={true}
                           onChangeText={password =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                password: password
                            }
                         }))} 
                         autoFocus={false}
                        />
                          <TitleSub
                         text={this.state.errors.password}
                         textColor="red"
                         />

<Input
                           underlineColorAndroid='transparent'
                           editable = {true}
                           autoCorrect={false}
                           placeholder='Retype password'
                           placeholderTextColor='#aeaaaa'
                           textColor='#000'
                           backgroundColor='#f9f9f9'
                           borderColor='#d2d2d2'
                           value={this.state.fields.repassword}
                           secureTextEntry={true}
                           onChangeText={repassword =>
                         this.setState(prevState =>
                          ({
                            fields: {
                                ...prevState.fields,
                                repassword: repassword
                            }
                         }))} 
                         autoFocus={false}
                        />
                          <TitleSub
                         text={this.state.errors.repassword}
                         textColor="red"
                         />
                        
                   

                    
                   <Button buttonAction={this.onRegister}  buttonBackgroundColor="#598bff" buttonText="Register" buttonTextColor="#fff"   />
                       
                        <TitleSub
                         text={this.props.message}
                         textColor="red"
                         />

                        <BlodLink 
                         text="If you have an account, SignIn!"
                         textColor="#598BFF"
                         boldAction={this.onPagechange}
                         bold={false}
                         padding={'7px'}

                        
                        />

      
       
                        </Container>
            </ImageBackground>
    );
    }

  


  
}




const mapDispatchToProps = dispatch => ({
  register: (data) => dispatch(actions.register(data)),
  page:(payload)=>dispatch(actions.page(payload))
 }); 
  
 const mapStateToProps = state => {
  return {
    user:state.authReducer.user,
    errors:state.authReducer.errors,
    loading:state.authReducer.loading,
    message:state.authReducer.messageReg,
    connection:state.network.isConnected
  }
};

export default withNavigation(connect(mapStateToProps,mapDispatchToProps)(connectAlert(Register)));

Register.propTypes = {
  user: PropTypes.object,
  errors: PropTypes.string,
  loading: PropTypes.bool,
  message: PropTypes.string,
  connection: PropTypes.bool
}  