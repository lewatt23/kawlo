import firebase, { auth, database, firestore } from "../../config/firebase";
import { setUpUserPresence } from './utils/userPresence';
var md5 = require('md5');







// Register the user using email and password
export function   registerUser(data) {
  const { email, password ,username } = data;
   return   auth.createUserWithEmailAndPassword(email.trim(), password.trim())
     .then(createdUser=>{
        
      return createdUser.user.updateProfile({
         displayName:username,
         photoURL:`http://gravatar.com/avatar/${md5(createdUser.user.email)}?d=identicon`
       }).then(() => {
        
        return createUserInDb(createdUser).then(() => createdUser)
        .catch((error) => console.log(error))
        
      })
  })
  .catch((error) => console.log(error))
}

// create user in the fireStore db
 createUserInDb = async (createdUser) => {
 
  database.ref('users').child(createdUser.user.uid).set({
    username:createdUser.user.displayName,
    rank:"new",
    avater:createdUser.user.photoURL
  })
}




//Sign the user in with their email and password
//not using 
export function login(data) {
  const { email, password } = data;
  auth.signInWithEmailAndPassword(email, password)
    .then((user) => getUser(user, callback))
    .catch((error) => callback(false, null, error));
}




// signOut funtion
export function signOut(callback) {
  auth.signOut()
    .then(() => {
      if (callback) callback(true, null, null)
    })
    .catch((error) => {
      if (callback) callback(false, null, error)
    });
}






// get User from the Database
export function getUser(user, callback) {
  firestore.collection('users').doc(user.uid).get()
    .then((doc) => {
      const data = { exists: doc.exists, user: doc.data() };

      callback(true, data, null);
    })
    .catch((error) => callback(false, null, error))
}


// send Email Verification
export function sendEmailVerification(callback) {
  auth.currentUser.sendEmailVerification()
    .then(() => callback(true))
    .catch((error) => callback(false, null, error))
}

// send password reset Email
export function sendPasswordResetEmail(email, callback) {
  auth.sendPasswordResetEmail(email)
    .then(() => callback(true, null, null))
    .catch((error) => callback(false, null, error))
}

// change current user's Password
export function changePassword(newPassword, callback) {
  auth.currentUser.updatePassword(newPassword)
    .then(() => callback(true, null, null))
    .catch((error) => callback(false, null, { message: error }))
}
