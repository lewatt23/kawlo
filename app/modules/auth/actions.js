import { takeLatest, put, call,select } from 'redux-saga/effects';
import * as t from './actionTypes';
import * as api from './api';
import firebase, { auth, database, firestore } from "../../config/firebase";
import {delay} from '../../utils/smallfunctions';

//delayed function

export const getConnectionState = (state) => state.network.isConnected;


//actions 

export  const login =(data) => {
     
  return {
      type:t.LOGGED_START,
      data:data,
      meta: {
        retry: true,
      },
  };


}
export  const notif =(data) => {
     
  return {
      type:t.NOTIF_START,
      data:data,
     
  };


}

export  const register =(data) => {
     
  return {
      type:t.CREATE_USER ,
      data:data,
      meta: {
        retry: true,
      },
  };


}
export  const logout =() => {
     
  return {
      type:t.CREATE_USER ,
      data:data,
      meta: {
        retry: true,
      },
  };


}

export  const page=(data) => {
     
  return {
      type:t.AuthPage,
      data
  };

}






//sagas


//funtion to  login user and get  his data
function* Loginfirebase(action) {
  try {
  
      
         yield put({ type: t.LOGGED_LOADING, payload: {loading:true,message:null} });

         const user = yield call([auth,auth.signInWithEmailAndPassword],action.data.email.trim(), action.data.password.trim());
        
         yield put({ type: t.LOGGED_IN, payload: {isLoggedIn: true,user:user,loading:false,messgae:null} });
          
         
     } catch (error) {
         
        yield put({ type: t.LOGGED_IN, payload: {message:error['message'],loading:false} });
        yield delay(10000);
        yield put({ type: t.LOGGED_IN, payload: {message:''} }); 
     }
 }


 //funtion to  register user  and  login  him one time 
 //@correct error not showing in the frontend during the testing phase
 function* Registerfirebase(action) {
  try {
   
         yield put({ type: t.CREATE_USER_SUCCESS, payload: {loading:true,messageReg:null} });

         const user = yield call(api.registerUser,action.data);
         
         yield put({ type: t.LOGGED_IN, payload: {isLoggedIn: true,user:user,loading:false} });

         
         
     } catch (error) {
        
         
         yield put({ type: t.CREATE_USER_FAILED, payload: {messageReg:error,loading:false} });
         yield delay(10000);
         yield put({ type: t.CREATE_USER_FAILED, payload: {messageReg:'',loading:false} });

     }
 }
 
 //funtion to  logout user and delete data
 function* Logoutfirebase() {
  try {
 
      
      yield put({ type: t.LOGGED_OUT, payload: {loading:false,isLoggedIn: false,user:null} });
         
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.LOGGED_IN, payload: {loading:false} });
     }
 }

//changes  page
 function* ChangePage(action) {
  try {
 
      
      yield put({ type: t.LOGGED_LOADING, payload: {loading:false,page:action.data.page} });
         
     } catch (error) {
         console.log('saga fail: ', error);
         
     }
 }
 function* NotificationAlert(action) {
  try {
 
      
      yield put({ type: t.NOTIF_END, payload: {notif:action.data} });
    

         
     } catch (error) {
         console.log('saga fail: ', error);
         
     }
 }
 


 export function* Register() {
     yield takeLatest(t.CREATE_USER, Registerfirebase);
 }
 
 export function* Login() {
     yield takeLatest(t.LOGGED_START, Loginfirebase);
 }
 

 export function* Logout() {
     yield takeLatest(t.LOGGED_START, Logoutfirebase);
 }
 export function* Notification() {
     yield takeLatest(t.NOTIF_START, NotificationAlert);
 }

 export function* Page() {
    yield takeLatest(t.AuthPage, ChangePage);
}










