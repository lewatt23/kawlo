import * as t from './actionTypes';
import * as api from './api';


export function createUser(user, successCB, errorCB) {
  return (dispatch) => {
    api.createUserInDb(user, function (success, data, error) {
      if (success) {
        dispatch({ type: t.LOGGED_IN, payload: { user } });
        successCB();
      } else if (error) errorCB(error)
    });
  };
}

// Check if user Logged In or not
export function checkLoginStatus(callback) {
  return (dispatch, store) => {
    auth.onAuthStateChanged((user) => {
      let isLoggedIn = (user !== null);

      if (isLoggedIn) {
        dispatch({ type: t.LOGGED_IN, });

        // setup Presence
        dispatch(checkPresence((state) => console.log((state) ? 'online' : 'offline')));

        callback(isLoggedIn);
        //});
      } else {
        dispatch({ type: t.LOGGED_OUT });
        callback(isLoggedIn);
      }
    });
  };
}

// Checks if user currently online or not.
export function checkPresence(callback) {
  return (dispatch) => {
    api.onPresenceStateChanged((data) => {
      let isOnline = data.state;

      if (isOnline) {
        dispatch({ type: t.SET_ONLINE, payload: { lastSeen: data.last_changed } });
      } else {
        dispatch({ type: t.SET_OFFLINE, payload: { lastSeen: data.last_changed } })
      }

      callback(isOnline);
    })
  }
}
