//sample  import

import * as actions from './actions';
import * as constants from './constants';
import * as actionTypes from './actionTypes';
import reducer from './reducer';
import  Questions from './scenes/Questions';
import  Question from './common/Question';
import  Category from './common/Category';

// import * as theme from '../../styles/theme';

export { actions, constants, actionTypes, reducer ,Category,Question, Questions};
