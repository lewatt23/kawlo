import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { UtilStyles } from "../../../components";

export default class Questions extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity
        style={{ marginHorizontal: 20 }}
        onPress={() => navigation.openDrawer()}
      >
        <Icon name="menu" color={"#FF7F50"} size={20} />
      </TouchableOpacity>
    )
  });
  render() {
    const Pagenotes = [
      {
        Icon: "currency-usd",
        name: "Accounting",
        url: "http://cameroongcerevision.com/wp-json/wp/v2/posts?categories=1"
      },
      {
        Icon: "dna",
        name: "Biology",
        url: ""
      },
      {
        Icon: "laptop",
        name: "Computer /ICT",
        url: ""
      },
      {
        Icon: "test-tube",
        name: "Chemistry",
        url: ""
      },
      {
        Icon: "cash",
        name: "Commerce",
        url: ""
      },
      {
        Icon: "human-female-boy",
        name: "Citizenship",
        url: ""
      },
      {
        Icon: "chart-bar",
        name: "Economics",
        url: ""
      },
      {
        Icon: "book-open-page-variant",
        name: "Literature",
        url: ""
      },
      {
        Icon: "pencil",
        name: "English",
        url: ""
      },
      {
        Icon: "book-open",
        name: "French",
        url: ""
      },
      {
        Icon: "globe-model",
        name: "Geography",
        url: ""
      },
      {
        Icon: "earth",
        name: "Geology",
        url: ""
      },
      {
        Icon: "notebook",
        name: " History",
        url: ""
      },
      {
        Icon: "food-variant",
        name: " Home Economics",
        url: ""
      },
      {
        Icon: "calculator",
        name: "Mathmatics",
        url: ""
      },
      {
        Icon: "nuke",
        name: "Physics",
        url: ""
      },
      {
        Icon: "account",
        name: "Philosophy/logic",
        url: ""
      },
      {
        Icon: "christianity",
        name: "Religious Studies",
        url: ""
      }
    ];

    return (
      <ScrollView>
        <View style={styles.container}>
          {Pagenotes !== null ? (
            Pagenotes.map(el => {
              return (
                <View key={el.name.toString()}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("Category", {
                        url: el.url
                      })
                    }
                    style={UtilStyles.feedsBottom}
                  >
                    <View>
                      <Icon name={el.Icon} color={"#FF7F50"} size={40} />
                    </View>

                    <View style={UtilStyles.spaceVertical}>
                      <Text style={UtilStyles.headingtitle}>{el.name}</Text>
                    </View>
                    <View>
                      <Text>
                        <Icon
                          name="chevron-right"
                          color={"#FF7F50"}
                          size={40}
                        />
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={UtilStyles.line} />
                </View>
              );
            })
          ) : (
            <Text>Loading...</Text>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
 
  }
});
