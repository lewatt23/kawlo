import React from 'react';
import {Dimensions } from 'react-native';
import { withNavigation } from 'react-navigation'; 
import HTML from 'react-native-render-html';
import {CardNotes} from './../../../components/molecules/card/card';




class ListQuestions extends React.Component {
 
 



  render() {
    const {notes} = this.props
    //preparing data
    const title = !!notes.item?notes.item.title.rendered:'';
    const image = !!notes.item?notes.item.image:'';
    const content = <HTML
      html={notes.item.excerpt.rendered}
      imagesMaxWidth={Dimensions.get('window').width}
    />;
 
   



    return (
      

          <CardNotes
          title={title}
          image={image}
          text={content}
          cardAction={()=> this.props.navigation.navigate("QuestionPaper",{ notes: this.props.notes  })}
          />
          
          
   
       

    );
  }
}



export default withNavigation(ListQuestions);