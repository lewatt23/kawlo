import firebase, { auth, database, firestore } from "../../config/firebase";
import { setUpUserPresence } from './utils/userPresence';

let userPresenceListener;

export function onPresenceStateChanged(callback) {
  userPresenceListener = setUpUserPresence({ firebase, auth, firestore, database }, callback)
}

export function unSubscribePresenceListener() {
  if (typeof userPresenceListener === 'function') {
    userPresenceListener();
  }
}

// Register the user using email and password
export function registerUser(data, callback) {
  const { email, password } = data;
  auth.createUserWithEmailAndPassword(email, password)
    .then((user) => callback(true, user, null))
    .catch((error) => callback(false, null, error));
}

// create user in the fireStore db
export function createUserInDb(user, callback) {
  firestore.collection('users').doc(user.uid).set(user)
    .then(() => callback(true, null, null))
    .catch((error) => callback(false, null, { message: error }))
}


//Sign the user in with their email and password
export function login(data, callback) {
  const { email, password } = data;
  auth.signInWithEmailAndPassword(email, password)
    .then((user) => getUser(user, callback))
    .catch((error) => callback(false, null, error));
}

// get User from the Database
export function getUser(user, callback) {
  firestore.collection('users').doc(user.uid).get()
    .then((doc) => {
      const data = { exists: doc.exists, user: doc.data() };

      callback(true, data, null);
    })
    .catch((error) => callback(false, null, error))
}

// send Email Verification
export function sendEmailVerification(callback) {
  auth.currentUser.sendEmailVerification()
    .then(() => callback(true))
    .catch((error) => callback(false, null, error))
}

// send password reset Email
export function sendPasswordResetEmail(email, callback) {
  auth.sendPasswordResetEmail(email)
    .then(() => callback(true, null, null))
    .catch((error) => callback(false, null, error))
}

// change current user's Password
export function changePassword(newPassword, callback) {
  auth.currentUser.updatePassword(newPassword)
    .then(() => callback(true, null, null))
    .catch((error) => callback(false, null, { message: error }))
}

export function signOut(callback) {
  auth.signOut()
    .then(() => {
      if (callback) callback(true, null, null)
    })
    .catch((error) => {
      if (callback) callback(false, null, error)
    });
}
