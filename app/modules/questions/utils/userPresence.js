export function setUpUserPresence(params, callback) {
  const
    { auth, database, firestore, firebase } = params;


  const uid = auth.currentUser.uid;

// reference to users specific status node
  const userStatusDatabaseRef = database.ref('/status/' + uid);

// reference to users specific status node on firestore
  const userStatusFireStoreRef = firestore.doc('/status/' + uid);

// Since the different databases use different server timeStamp values
// For Realtime Database
  const isOfflineForDatabase = {
    state: 'offline',
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  const isOnlineForDatabase = {
    state: 'online',
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

// For Firestore
  const isOfflineForFirestore = {
    state: 'offline',
    last_changed: firebase.firestore.FieldValue.serverTimestamp(),
  };

  const isOnlineForFirestore = {
    state: 'online',
    last_change: firebase.firestore.FieldValue.serverTimestamp(),
  };

// this reference to realtime database emits true when user is online
// and false for online state.
  database.ref('.info/connected').on('value', (snapshot) => {

    if (!snapshot.val()) {
      // Immediately user goes offline, we set firestore cache

      userStatusFireStoreRef.set(isOfflineForFirestore);
      return;
    }

    // this onDisconnect, will set this isOfflineForDatabase on the userStatusRef
    // only when the user disconnects, but while that doesn't happen, it sets the
    // isOnlineForDatabase at the status location.
    userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase)
      .then(() => {
        userStatusDatabaseRef.set(isOnlineForDatabase);

        // set firestore online when we come online
        userStatusFireStoreRef.set(isOnlineForFirestore);
      });
  });

  // listen for changes and dispatch user presence
  return userStatusFireStoreRef.onSnapshot((doc) => {
    const data = doc.data();

    callback(data)
  });
}



