import * as t from './actionTypes';

let initialState = { 
  accounting:null,
  biology:null,
  computer:null,
  chemistry:null,
  commercer:null,
  citizenship:null,
  economics:null,
  literature:null,
  english:null,
  french:null,
  geography:null,
  geology:null,
  history:null,
  home_economics:null,
  maths:null,
  physics:null,
  philosophy:null,
  religion:null

};

const authReducer = (state = initialState, action) => {
  switch (action.type) {

    case t.GET_PAST_QUESTIONS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_PAST_QUESTIONS_FAILED:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_PAST_QUESTIONS_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    default:
      return state;
  }
};

export default authReducer;
