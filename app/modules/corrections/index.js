//sample  import

import * as actions from './actions';
import * as constants from './constants';
import * as actionTypes from './actionTypes';
import reducer from './reducer';
import  Corrections from './scenes/Corrections';

// import * as theme from '../../styles/theme';

export { actions, constants, actionTypes, reducer , Corrections};
