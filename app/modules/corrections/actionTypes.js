export const LOGGED_IN = '[Auth] LOGGED_IN';
export const LOGGED_OUT = '[Auth] LOGGED_OUT';

export const CREATE_USER = '[Auth] Create User';

export const SET_ONLINE = '[Auth] SET_ONLINE';
export const SET_OFFLINE = '[Auth] SET_OFFLINE';
