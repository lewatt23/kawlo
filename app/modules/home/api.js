import firebase, { auth, database, firestore } from "../../config/firebase";
import Firebase from 'firebase';
import uuid from 'uuid';


//funtion used to create a new question
export async  function createQuestion(data){
  const {title,question,category,subject,user,uri} = data; 
  let image ="";

  if(uri !== null){
    // console.log(uri);
   image = await  uploadImages(uri);
 // console.log(image);
  }

  const key =   database.ref("questions").push().key;
   const newQuestion = {
         id:key,
         title:title,
         image:image,
         question:question,
         category:category,
         subject:subject,
         timestamp:Firebase.database.ServerValue.TIMESTAMP,
         likes:0,
         share:0,
         ans:0,
         createdBy:{
             name:user.displayName,
             avater:user.photoURL
         }
   }
   
 return  database.ref("questions").child(key)
   .update(newQuestion)
   .then(()=>{
      return  "Question added"
   }).catch((err) =>{
     //  console.log(err)
   })


}


//functions offline add questions
export async  function createQuestionOffline(data){
  const {title,question,category,subject,user,uri} = data; 

  const key =   database.ref("questions").push().key;
   const newQuestion = {
         id:key,
         title:title,
         image:uri,
         question:question,
         category:category,
         subject:subject,
         timestamp:Firebase.database.ServerValue.TIMESTAMP,
         likes:0,
         share:0,
         ans:0,
         createdBy:{
             name:user.displayName,
             avater:user.photoURL
         }
   }

   return newQuestion;


}



export const uploadImages = async (uri) =>{
     // Why are we using XMLHttpRequest? See:
  // https://github.com/expo/expo/issues/2402#issuecomment-443726662

  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
    });

    const ref = firebase.storage().ref().child('img/'+ uuid.v4());
    const snapshot = await ref.put(blob);
     // We're done with the blob, close and release it
     blob.close();
    
    return await snapshot.ref.getDownloadURL();
 }

export async function createReply(data){
    const {reply,item,user,uri} = data;
    let image ="";

    if(uri !== null){
      // console.log(uri);
     image = await  uploadImages(uri);
   // console.log(image);
    }
    const Answer = {
        timestamp:Firebase.database.ServerValue.TIMESTAMP,
        user:{
            id:user.uid,
            name:user.displayName,
            avater:user.photoURL
        },
        answer:reply,
        image:image,
        likes:0
    } 
   
    database.ref("answers").child(item.id)
    .push()
    .set(Answer)
    .then(()=>{
        // console.log(item);
        ansCount(item.id,item.ans);
    }).catch(err =>{
        
    })
}

// like  a question
export async function likeQuestion(data){
    const {id,likes} = data;
    return new Promise((resolve,reject)=> {
        database.ref("questions/"+ id).update({likes:likes+1});
     
    });
}
// ans count
export async function ansCount(id,ans){
   
    return new Promise((resolve,reject)=> {
        database.ref("questions/"+ id).update({ans:ans+1});
     
    });
}


//functions that get all questions from firebase

export  function getQuestion() {
    return new Promise((resolve,reject)=> {
        database.ref("questions")
        .orderByKey()
       .limitToLast(5)
       .once('value',(snap,err) =>{
            if(err){
                return reject(err);
            }
          return resolve(snap.val());
      });
     
    });
  
}

//getting other question set
export  function getQuestionP2(oldestKeyReference) {
    return new Promise((resolve,reject)=> {
        database.ref("questions")
        .orderByKey()
      .endAt(oldestKeyReference)
      .limitToLast(6)
       .once('value',(snap,err) =>{
            if(err){
                return reject(err);
            }
          return resolve(snap.val());
      });
     
    });
  
}


// function  that  get all  reply  for a question  from  firebase
export  function getReply(data) {
    const {item} = data;

    return new Promise((resolve,reject)=> {
        database.ref("answers").child(item.id).on('value',(snap,err) =>{
            if(err){
                return reject(err);
            }
          return resolve(snap.val());
      });
     
    });
  
}