import * as t from './actionTypes';
import * as api from './api';
import { takeLatest, put, call, select } from 'redux-saga/effects'
import { CALL_ALERT } from './../../components/Alert/actionTypes';



//setting init  value
let referenceToOldestKey ;

//state questio
export const getQuestState = (state) => state.questionReducer.questions;


export const delay = (ms) => new Promise(res => setTimeout(res, ms));


//actions 

export  const addQuestion =(data) => {
     
  return {
      type:t.CREATE_QUESTION_START,
      data:data,
      meta: {
        retry: true,
      },
  };


}
export  const addReply =(data) => {
     
  return {
      type:t.CREATE_REPLY_START,
      data:data,
      meta: {
        retry: true,
      },
  };


}
export  const getQuestion =() => {
     
  return {
      type:t.GET_QUESTION_START,
      meta: {
        retry: true,
      },
  
  };


}
export  const getReply =(data) => {
     
  return {
      type:t.GET_REPLY_START,
       data:data
  };


}



//sagas


//funtion to  login user and get  his data
function* addQuestionFirebase(action) {
  try {
         //add question frist in the queue before adding it online
         
         //const resultQuestions = yield call(api.createQuestionOffline,action.data);

         //add question to queue
         //yield put({ type: t.GET_QUESTION_SUCCESS, payload: {loading:false,questions:resultQuestions} });

        //now loading question to firebase
        const result = yield call(api.createQuestion,action.data);
        

         yield put({ type: t.CREATE_QUESTION_SUCCESS, payload: {loading:false} });
         yield put({ type: t.GET_QUESTION_FAILED, payload: {loading:true} });

        const resultapi = yield call(api.getQuestion);
        
       
       //lazy loading implementation
       let arrayOfKeys = Object.keys(resultapi).sort().reverse();

       let results = arrayOfKeys
       .map((key) => resultapi[key]);

       referenceToOldestKey = arrayOfKeys[arrayOfKeys.length-1];
      
    

       yield put({ type: t.GET_QUESTION_SUCCESS, payload: {loading:false,questions:results} });
       yield put({ type: t.GET_QUESTION_SUCCESS, payload: {loading:false,questions:results} });



      
         
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.CREATE_QUESTION_FAILED, payload: {loading:false} });
     }
 }

 //saga that  add a reply  in firebase
function* addReplyFirebase(action) {
  try {
  
         yield put({ type: t.CREATE_REPLY_SUCCESS, payload: {loadaddreply:true} });

         const result = yield call(api.createReply,action.data);
        

         yield put({ type: t.CREATE_REPLY_SUCCESS, payload: {loadaddreply:true} });

         const reply = yield call(api.getReply,action.data);
         yield put({ type: t.GET_REPLY_SUCCESS, payload: {loadaddreply:false,answers:reply} });
         

         
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.CREATE_REPLY_FAILED, payload: {loadaddreply:false} });
     }
 }


 // saga  that  get questions 
function* getQuestionFirebase() {
   
    let result='';

  try {
  
 

        if (referenceToOldestKey === undefined){

            
       yield put({ type: t.GET_QUESTION_FAILED, payload: {loading:true} });

        result = yield call(api.getQuestion);
        
       
       //lazy loading implementation
       let arrayOfKeys = Object.keys(result).sort().reverse();

       let results = arrayOfKeys
       .map((key) => result[key]);

       referenceToOldestKey = arrayOfKeys[arrayOfKeys.length-1];

      

       yield put({ type: t.GET_QUESTION_SUCCESS, payload: {loading:false,questions:results} });

       }
       else{
        yield put({ type: t.GET_QUESTION_FAILED, payload: {loadingmore:true} });   

        result = yield call(api.getQuestionP2,referenceToOldestKey);

  
        
        let arrayOfKeys = Object.keys(result).sort().reverse().slice(1);

        let results = arrayOfKeys.map((key) => result[key]);
        
        referenceToOldestKey = arrayOfKeys[arrayOfKeys.length-1];

        let oldquest = yield select(getQuestState);
       
        let res =  Array.from(new Set(oldquest.concat(results)));

        yield put({ type: t.GET_QUESTION_SUCCESS, payload: {loadingmore:false,questions:res} });
      }   
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.GET_QUESTION_FAILED, payload: {loading:false} });
         yield put({ type: t.GET_QUESTION_FAILED, payload: {loadingmore:false} });
     }
 }

//saga  that gets all  reply @params{questionid}

function* getReplyFirebase(action) {
  try {
  
        
         yield put({ type: t.GET_QUESTION_FAILED, payload: {loadget:true,answers:null}});
        const result = yield call(api.getReply,action.data);
         yield put({ type: t.GET_REPLY_SUCCESS, payload: {loadget:false,answers:result} });
         
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.GET_REPLY_FAILED, payload: {loadget:false} });
     }
 }


 
export function* AddQuestion() {
    
  yield takeLatest(t.CREATE_REPLY_START, addReplyFirebase);
}
export function* AddReply() {
  yield takeLatest(t.CREATE_QUESTION_START, addQuestionFirebase);
}
export function* GetQuestion() {
  yield takeLatest(t.GET_QUESTION_START, getQuestionFirebase);
}
export function* GetReply() {
  yield takeLatest(t.GET_REPLY_START, getReplyFirebase);
}