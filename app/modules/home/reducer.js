import * as t from './actionTypes';

let initialState = { loadget:false,loadaddreply:false,loading:false,message:"",questions:"",answers:"" };

const questionReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.CREATE_QUESTION_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.CREATE_QUESTION_FAILED:
       state = Object.assign({}, state, { ...action.payload });
      return state;

      case t.CREATE_QUESTION_START:
       state = Object.assign({}, state, { ...action.payload });
      return state;
    case t.CREATE_REPLY_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.CREATE_REPLY_FAILED:
       state = Object.assign({}, state, { ...action.payload });
      return state;

      case t.CREATE_REPLY_START:
       state = Object.assign({}, state, { ...action.payload });
      return state;


    case t.GET_QUESTION_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_QUESTION_FAILED:
    state = Object.assign({}, state, { ...action.payload });
      return state;


     case t.GET_QUESTION_START:
    state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_REPLY_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_REPLY_FAILED:
    state = Object.assign({}, state, { ...action.payload });
      return state;


     case t.GET_REPLY_START:
    state = Object.assign({}, state, { ...action.payload });
      return state;

    default:
      return state;
  }
};

export default questionReducer;
