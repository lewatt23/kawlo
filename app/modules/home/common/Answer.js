import React from "react";
import {
  Alert,
  View,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import { connect } from "react-redux";
import * as actions from "../actions";
import { withNavigation } from "react-navigation";
import { KeyboardAvoidingView } from "react-native";
import Container, { Row ,Columns} from "../../../components/atoms/container/container";
import { TitleSub } from "../../../components/atoms/typography/typography";
import { InputTextArea } from "../../../components/atoms/inputs/input";
import Button from './../../../components/atoms/buttons/button';


class Answer extends React.Component {
  state = {
    visible: false,
    showEmoticons: true,
    uris: {
      uri: null
    },
    fields: {
      reply: ""
    },
    errors: {}
  };

  addAnswer = () => {
    // console.log(this.state.errors.reply);
    if (this.validateForm()) {
      if (this.props.user !== null) {
        let ans = {
          ...this.props.question,
          ...this.state.fields,
          ...this.props.user,
          ...this.state.uris
        };
        this.props.addReply(ans);

        //    this.props.getReply(this.props.question);
        let fields = {};
        fields["reply"] = "";

        this.setState({ fields: fields });
      } else {
        this.checkAuth();
      }
    }
  };

  pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [3, 2]
    });

    if (!result.cancelled) {
      this.setState(prevState => ({
        uris: {
          ...prevState.uri,
          uri: result.uri
        }
      }));
    }
  };

  //function use to validate form
  validateForm = () => {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["reply"]) {
      formIsValid = false;
      errors["reply"] = "*Please enter your Answer.";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  };

  checkAuth = () => {
    Alert.alert(
      "login Erro",
      "Sorry you have to be logged in to contribute!",
      [
        {
          text: "Login",
          onPress: () => this.props.navigation.navigate("Auth")
        },
        {
          text: "Register",
          onPress: () => this.props.navigation.navigate("Auth"),
          style: "cancel"
        },
        { text: "cancel", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  };
  render() {
    if (this.props.loading) {
      return <ActivityIndicator size="large" color="#0000ff" />;
    }

    return (
      <KeyboardAvoidingView  behavior="padding" enabled>
        <View>
          <Row>
            <Columns>
              <TouchableOpacity onPress={this.pickImage}>
                <Icon
                  name="camera"
                  size={20}
                  color="#5d95d9"
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon
                  name="emoticon"
                  size={20}
                  color="#fb8c00"
                />
              </TouchableOpacity>
            </Columns>

            <InputTextArea
              underlineColorAndroid="transparent"
              multiline={true}
              numberOfLines={3}
              editable={true}
              autoCorrect={false}
              placeholder="Enter your answer here"
              placeholderTextColor="#757575"
              textColor="#000"
              backgroundColor="#f9f9f9"
              borderColor="#d2d2d2"
              value={this.state.fields.reply}
              onChangeText={reply =>
                this.setState(prevState => ({
                  fields: {
                    ...prevState.fields,
                    reply: reply
                  }
                }))
              }
              autoFocus={false}
            />
          </Row>

   
          
       <TitleSub text={this.state.errors.reply} textColor="red" />
          <Row>
     
       <Button
            buttonAction={this.addAnswer}
            buttonBackgroundColor="#598bff"
            buttonText="Answer question"
            buttonTextColor="#fff"
          />
       

    
         
          </Row>
        </View>
      </KeyboardAvoidingView>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  addReply: data => dispatch(actions.addReply(data))
});
const mapStateToProps = state => {
  return {
    user: state.authReducer.user,
    loading: state.questionReducer.loadaddreply
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Answer)
);
