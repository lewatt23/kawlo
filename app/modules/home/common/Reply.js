import React from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import { timeFromNow } from "../../../utils/smallfunctions";
import styled from "styled-components";
import Title from "./../../../components/atoms/typography/typography";
import {
  CardImage,
  TextItalicCard,
  TextCard,
  ImageCard
} from "./../../../components/molecules/card/card";
import {
  TextSubNormal
} from "../../../components/atoms/typography/typography";

class Reply extends React.Component {
  render() {
    //Preparing data
    const { item } = this.props.reply;
    const author = !!item.user ? item.user.name : "";
    const answer = !!item.answer ? item.answer :""  
    const avater = !!item.user.avater ? item.user.avater : "";
    const image = !!item.image ? item.image : "";
    const time = !!item.timestamp ? timeFromNow(item.timestamp) : "";


    return (
      <View style={{paddingHorizontal:30,paddingVertical:2}}>

   
          <Row>
            <Columns>
              <ImageCard source={{ uri: avater }} />
             
            </Columns>
            <Columns>
              <TextCard fontSize={"12px"}>{"By " + author}</TextCard>
              <TextItalicCard>{time}</TextItalicCard>
            </Columns>
          </Row>
       
          <TextSubNormal text={answer} textColor="#000" />

            {!!image && (
              <CardImageContainer
                onPress={() =>
                  this.props.navigation.navigate("ImageView", {
                    image:image
                  })
                }
              >
                <CardImage source={{ uri: image }} />
              </CardImageContainer>
            ) }
         
        <Line></Line>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff"
  }
});

const CardImageContainer = styled.TouchableOpacity``;


const Line = styled.View`
  height: 0.5;
  background-color: #ebebeb;
  margin: 10px;

`;
const  Columns = styled.View`
 
    flex-direction:column;
    justify-content: flex-start; 
    margin-bottom:10px;

`;

const Row = styled.View`
  
    flex-direction: row;
    justify-content: flex-start; 
  
    
`;

const mapStateToProps = state => {
  return {
    loading: state.questionReducer.loadget
  };
};

export default withNavigation(connect(mapStateToProps)(Reply));
