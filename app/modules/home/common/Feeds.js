import React from 'react';
import { withNavigation } from 'react-navigation'; 
import * as api from '../api';
import Card from './../../../components/molecules/card/card';
import PropTypes from 'prop-types';
import { timeFromNow } from '../../../utils/smallfunctions';







class Feeds extends React.Component {

  
  


state = {
    vote:true,
    likes:this.props.question.item.likes,
}



 
componentDidMount(){
   

}

likepost=(data)=>{
    if(this.state.vote){
        this.setState({likes:this.state.likes+1});
        api.likeQuestion(data);
        this.setState({vote:false});
    }
    
}

  render() {
 
     //preparing objects from DB
        const {item} = this.props.question;
     
         let title = item.title?item.title:'';
         let image = item.image?item.image:'';
         let time =  item.timestamp?timeFromNow(item.timestamp):'';
         let user_name = item.createdBy.name?item.createdBy.name:'';
         let user_avater = item.createdBy.avater?item.createdBy.avater:''
         let text = item.question?item.question:'';
         let subject = item.subject?item.subject:'';
         let cat = item.category?item.category:'';
         let ans = item.ans?item.ans:0;
         let likes = this.state.likes?this.state.likes:0;

       


    return (
      

         
         
                
                    <Card 
                     title={title}
                     image={image}
                     text={text}
                     likes={likes}
                     reply={ans}
                     time={time}
                     user_name={user_name}
                     user_avater={user_avater}
                     subject={subject}
                     cat={cat}
                     cardAction={ ()=> this.props.navigation.navigate("Answers",{ question: this.props.question  })}
                     cardlikeAction={ ()=> this.likepost(this.props.question.item)}

                    />


          
 
                

    );
  }
}

export default withNavigation(Feeds);

Feeds.propTypes = {
  question: PropTypes.object
 
}  