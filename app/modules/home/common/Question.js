import React from "react";
import {
  Alert,
  ScrollView,
  View,
  Picker,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import { connect } from "react-redux";
import * as actions from "../actions";

import { UtilStyles } from "../../../components";
import { withNavigation } from "react-navigation";
import Container, {
  Row,
  Columns
} from "./../../../components/atoms/container/container";
import Input, { InputTextArea } from "./../../../components/atoms/inputs/input";
import Title, {
  TitleSub
} from "./../../../components/atoms/typography/typography";
import Button from "./../../../components/atoms/buttons/button";

class Question extends React.Component {
  state = {
    visible: false,
    uris: {
      uri: null
    },
    fields: {
      title: "",
      question: "",
      category: "",
      subject: ""
    },
    errors: {}
  };
  componentDidMount() {}

  //method to add a question
  addQuestion = () => {
    if (this.validateForm()) {
      if (this.props.user !== null) {
        let info = {
          ...this.state.fields,
          ...this.props.user,
          ...this.state.uris
        };
        this.props.onAddQuestions(info);
        let fields = {};
        fields["category"] = "";
        fields["subject"] = "";
        fields["title"] = "";
        fields["question"] = "";
        this.setState({ fields: fields });
        this.props.navigation.goBack(null);
      } else {
        this.checkAuth();
      }
    }
  };

  pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [3, 2]
    });

    if (!result.cancelled) {
      this.setState(prevState => ({
        uris: {
          ...prevState.uri,
          uri: result.uri
        }
      }));
    }
  };

  //function use to validate form
  validateForm = () => {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["question"]) {
      formIsValid = false;
      errors["question"] = "*Please enter your question.";
    }
    if (!fields["subject"]) {
      formIsValid = false;
      errors["subject"] = "*Please enter your subject.";
    }
    if (!fields["category"]) {
      formIsValid = false;
      errors["category"] = "*Please enter your category.";
    }

    if (!fields["title"]) {
      formIsValid = false;
      errors["title"] = "*Please enter your title.";
    }

    this.setState({
      errors: errors
    });
    return formIsValid;
  };

  checkAuth = () => {
    Alert.alert(
      "login Erro",
      "Sorry you have to be logged in to contribute!",
      [
        {
          text: "Login",
          onPress: () => {
            this.props.navigation.navigate("Auth");
           
          }
        },
        {
          text: "Register",
          onPress: () => {
            this.props.navigation.navigate("Auth");
          },
          style: "cancel"
        },
        { text: "cancel"}
      ],
      { cancelable: false }
    );
  };

  render() {
    return (
      <Container backgroundColor="#fff">
        <ScrollView>
          <View style={{ paddingTop: 30 }}>
            <Row>
              <View />

              <Input
                underlineColorAndroid="transparent"
                editable={true}
                autoCorrect={false}
                placeholder="Question Title"
                placeholderTextColor="#757575"
                textColor="#000"
                backgroundColor="#f9f9f9"
                borderColor="#d2d2d2"
                value={this.state.fields.title}
                onChangeText={title =>
                  this.setState(prevState => ({
                    fields: {
                      ...prevState.fields,
                      title: title
                    }
                  }))
                }
                autoFocus={false}
              />
            </Row>
            <TitleSub text={this.state.errors.title} textColor="red" />
          </View>

          <View>
            <Row>
              <Columns>
                <TouchableOpacity onPress={this.pickImage}>
                  <Icon name="camera" size={20} color="#5d95d9" />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon name="emoticon" size={20} color="#fb8c00" />
                </TouchableOpacity>
              </Columns>

              <InputTextArea
                underlineColorAndroid="transparent"
                multiline={true}
                numberOfLines={10}
                editable={true}
                autoCorrect={false}
                placeholder="Enter your question here"
                placeholderTextColor="#757575"
                textColor="#000"
                backgroundColor="#f9f9f9"
                borderColor="#d2d2d2"
                value={this.state.fields.question}
                onChangeText={question =>
                  this.setState(prevState => ({
                    fields: {
                      ...prevState.fields,
                      question: question
                    }
                  }))
                }
                autoFocus={false}
              />
            </Row>

            <TitleSub text={this.state.errors.question} textColor="red" />
          </View>

          <View style={{ justifyContent: "center" }}>
            <Picker
              style={UtilStyles.dropdown}
              selectedValue={this.state.fields.subject}
              prompt="Select the subject"
              onValueChange={subject =>
                this.setState(prevState => ({
                  fields: {
                    ...prevState.fields,
                    subject: subject
                  }
                }))
              }
              mode="dropdown"
            >
              <Picker.Item label="Biology" value="Biology" />
              <Picker.Item label="Human Biology" value="Human Biology" />
              <Picker.Item label="Chemistry" value="Chemistry" />
              <Picker.Item label="Physics" value="Chemistry" />
              <Picker.Item label="Phylosopy" value="Phylosopy" />
              <Picker.Item label="History" value="History" />
              <Picker.Item label="Geography" value="Geography" />
              <Picker.Item label="History" value="History" />
              <Picker.Item label="Math" value="Math" />
              <Picker.Item label="A math" value="A math" />
              <Picker.Item label="English" value="English " />
              <Picker.Item label="Futher math" value="Futher math" />
              <Picker.Item label="others" value="others" />
            </Picker>
            <TitleSub text={this.state.errors.subject} textColor="red" />
          </View>
         
          <View style={{ justifyContent: "flex-end" }}>
            <Picker
              style={UtilStyles.dropdown}
              selectedValue={this.state.fields.category}
              prompt="Select category"
              onValueChange={category =>
                this.setState(prevState => ({
                  fields: {
                    ...prevState.fields,
                    category: category
                  }
                }))
              }
              mode="dropdown"
            >
              <Picker.Item label="A level" value="A level" />
              <Picker.Item label="O level" value="O level" />
              <Picker.Item label="others" value="Others" />
            </Picker>

            <TitleSub text={this.state.errors.category} textColor="red" />
          </View>

          <View style={{ justifyContent: "flex-end" }}>
            <Button
              buttonAction={this.addQuestion}
              buttonBackgroundColor="#598bff"
              buttonText="Ask question"
              buttonTextColor="#fff"
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onAddQuestions: data => dispatch(actions.addQuestion(data))
});
const mapStateToProps = state => {
  return {
    user: state.authReducer.user
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Question)
);
