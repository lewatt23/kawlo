import React from "react";
import {
  View,
  FlatList,
  ScrollView,
  Image,
  ActivityIndicator
} from "react-native";

import { withNavigation } from "react-navigation";
import styled from "styled-components";
import Reply from "./Reply";
import Answer from "./Answer";
import { connect } from "react-redux";
import * as actions from "../actions";
import { timeFromNow } from "../../../utils/smallfunctions";
import {
  TextSubNormal
} from "../../../components/atoms/typography/typography";
import Title from "./../../../components/atoms/typography/typography";
import {
  CardImage,
  Card,
  CardButtom,
  Badge,
  BadgeText,
  TextItalicCard,
  TextCard,
  ImageCard
} from "./../../../components/molecules/card/card";

class Answers extends React.Component {
  state = {
    modalVisible: false,
    question: this.props.navigation.getParam("question", "NO-QUESTIONS")
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  componentDidMount() {
    this.props.getReply(
      this.props.navigation.getParam("question", "NO-QUESTIONS")
    );
  }

  listEmptyComponent = () => {
    return (
      
   <View style={{backgroundColor:'#fff',height:250}}>
       <Image
        source={require("../../../../assets/no.png")}
        style={{ width: "100%", height: "100%" }}
      />

       </View>


  
    );
  };
  check = () => {
    return this.props.answers
      ? Object.values(this.props.answers)
      : this.props.answers;
  };


  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#fff"
    },
    title: 'Answers',
   
  });

  render() {
    //preparing data

    const { item } = this.state.question;
    const title = item.title ? item.title : "";
    const author = item.createdBy ? item.createdBy.name : "";
    const text = item.question ? item.question : "";
    const cat = item.category ? item.category : "";
    const subject = item.subject ? item.subject : "";
    const avater = item.createdBy.avater ? item.createdBy.avater : "";
    const image = item.image ? item.image : "";
    const time = item.timestamp ? timeFromNow(item.timestamp) : "";
    let count = this.props.answers ? Object.values(this.props.answers).length : 0;

    return (
     
        <View style={{backgroundColor:"#fff",height:'100%'}}>
           <ScrollView>
          <View style={{ padding: 10 }}>
            <Title text={title} textColor="#000" />
             { !!image && 

                 
            <CardImageContainer
              onPress={() =>
                this.props.navigation.navigate("ImageView", {
                  image: this.state.question.item.image
                })
              }
            >
              <CardImage source={{ uri: image }} />
            </CardImageContainer>
            
          
             } 


            <TextSubNormal text={text} textColor="#000" />

            <CardButtom>
              <Row>
                <Columns>
                  <Row>
                    <Columns>
                      <ImageCard source={{ uri: avater }} />
                     
                    </Columns>
                    <Columns>
                      <TextCard fontSize={"12px"}>{"By " + author}</TextCard>
                      <TextItalicCard>{time}</TextItalicCard>
                    </Columns>
                 
                  </Row>
                </Columns>

                <Columns>
                  <Badge level={cat}>
                    <BadgeText>{cat + "-" + subject}</BadgeText>
                  </Badge>
                </Columns>
              </Row>
            </CardButtom>

              <Line>
                <Title text={count + " Anwers for this question"} textColor="#000" />
            </Line>
            <Line></Line>
          </View>


        


          <View>
            <Answer
              getReply={this.props.getReply}
              question={this.state.question}
            />

          </View>
          <Line></Line>




          {!this.props.loading ? (
            <View style={{backgroundColor:'#fff'}}>
              <FlatList
                data={this.check()}
                renderItem={answer => {
                  return (
                    <View>
                      <Reply reply={answer} />
                    </View>
                  );
                }}
                ListEmptyComponent={this.listEmptyComponent}
                keyExtractor={(answer, index) => index.toString()}
              />
            </View>
          ) : (
            <View style={{backgroundColor:'#fff'}}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          )}
            </ScrollView>
        </View>
    
    );
  }
}

const CardImageContainer = styled.TouchableOpacity``;
const Line = styled.View`
height:0.5;
background-color:#ebebeb;
margin:10px;
margin-top:20px;
`;

const  Columns = styled.View`
 
    flex-direction:column;
    justify-content: flex-start; 
  

`;

const Row = styled.View`
  
    flex-direction: row;
    justify-content: flex-start; 
  
    
`;


const mapDispatchToProps = dispatch => ({
  getReply: data => dispatch(actions.getReply(data))
});
const mapStateToProps = state => {
  return {
    user: state.authReducer.user,
    answers: state.questionReducer.answers,
    loading: state.questionReducer.loadget
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Answers)
);
