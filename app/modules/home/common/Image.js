import React from 'react';
import { StyleSheet, Text, View , TouchableOpacity,Image,ActivityIndicator  } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {UtilStyles} from '../../../components';
import moment from 'moment';
import {connect} from 'react-redux';
import * as actions from '../actions';
import { withNavigation } from 'react-navigation';

class ImageView extends React.Component {

  state = {
    modalVisible: false,
    image:this.props.navigation.getParam('image', 'https://via.placeholder.com/300.png/09f/fff'),
    
  };


  componentDidMount(){
    
     
    this.setState({image:this.props.navigation.getParam('image', 'https://via.placeholder.com/300.png/09f/fff')});
 }
  render() {
    

  
   
    return (
      
        <View style={styles.container}>

       
             
                    <View >
                        <Image source={{uri:this.state.image}}
                        
                        style={{ aspectRatio: 3/2 }}
                        />
                      </View>
                      
               </View>

   
       

    );
  }
}

const styles = StyleSheet.create({
  container: {
  backgroundColor: '#000',
   padding:10 ,
   flex:1,
   alignContent: 'center',
  }
});
const mapStateToProps = state => {
  return {
 
    loading:state.questionReducer.loadget,
  }
};

export default  withNavigation(connect(mapStateToProps)(ImageView));