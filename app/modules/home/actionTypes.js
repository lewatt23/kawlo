export const CREATE_QUESTION_START = 'CREATE_QUESTION_START';
export const CREATE_QUESTION_SUCCESS = 'CREATE_QUESTION_SUCCESS ';
export const CREATE_QUESTION_FAILED  = 'CREATE_QUESTION_FAILED ';

export const CREATE_REPLY_START = 'CREATE_REPLY_START';
export const CREATE_REPLY_SUCCESS = 'CREATE_REPLY_SUCCESS ';
export const CREATE_REPLY_FAILED  = 'CREATE_REPLY_FAILED ';

export const GET_QUESTION_START = 'GET_QUESTION_START';
export const GET_QUESTION_SUCCESS = 'GET_QUESTION_SUCCESS ';
export const GET_QUESTION_FAILED  = 'GET_QUESTION_FAILED ';


export const GET_REPLY_START = 'GET_REPLY_START';
export const GET_REPLY_SUCCESS = 'GET_REPLY_SUCCESS ';
export const GET_REPLY_FAILED  = 'GET_REPLY_FAILED ';


