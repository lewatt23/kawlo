import React, { Component } from 'react';

import { Text, View, StyleSheet, Image,TouchableOpacity } from 'react-native';
import { withNavigation,NavigationActions } from 'react-navigation';
import {connect} from 'react-redux';
import {UtilStyles,buttons } from '../../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';



const Pagenotes = 
[
{
  "Icon":"pencil",
   "name":"My Questions",
   "url":"page"
},
{
  "Icon":"bell",
   "name":"Notifications",
   "url":""
},
{
  "Icon":"account-multiple",
   "name":"Friends",
   "url":""
},
{
  "Icon":"settings",
   "name":"Settings",
   "url":"Profile"
},
{
  "Icon":"power",
   "name":"Logout",
   "url":""
}
];



 class DrawerContent extends Component {

 

  
    render() {
      const {user} = this.props;

        return (
          <View style={styles.container}>
              <View style={UtilStyles.header__drawer}></View>
              <Image style={UtilStyles.avatar__drawer} source={{uri:!!user? user.user.photoURL:'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
              <View style={UtilStyles.body__drawer}>
  
                   
                   

          {Pagenotes !== null? 

Pagenotes.map(el =>{

return(
<View key={el.name.toString()}>

 <TouchableOpacity 
   onPress={() => this.props.navigation.navigate(el.url,{
     url: el.url
   })}
 style={UtilStyles.feedsBottom}>
   <View>
       <Icon name={el.Icon} color={'#FF7F50'} size={30} />
     </View>
     
       <View style={UtilStyles.spaceVertical}>
         <Text style={UtilStyles.headingtitle}>
         {el.name}
           </Text> 
     </View>
     <View>
         <Text>
             <Icon name="chevron-right" color={'#FF7F50'} size={40} />
           </Text> 
     </View>
       

 </TouchableOpacity>
 <View style={UtilStyles.line}></View>
 </View>

) 
})


: 
<Text>
  Loading
</Text>


}
                 
                
            </View>
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
    
    });

    const mapStateToProps = state => {
      return {
        user:state.authReducer.user,
      }
    };
    export default  withNavigation(connect(mapStateToProps)(DrawerContent));