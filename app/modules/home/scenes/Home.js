import React from "react";
import {
  ActivityIndicator,
  View,
  Text,
  TouchableOpacity,
  FlatList,

} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Feeds from "../common/Feeds";
import { connect } from "react-redux";
import * as actions from "../actions";
import PropTypes from "prop-types";

import Container, {
  ContainerBackground
} from "../../../components/atoms/container/container";

class Home extends React.Component {
  state = {
    modalVisible: false,
    count: 0
  };



  componentDidMount() {
    //get  new questions when connected only
    if (this.props.connection) {
      this.props.getQuestion();
    }


  }

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#fff"
    },
    headerTintColor: "#000",
    headerLeft: (
      <TouchableOpacity
        style={{ marginHorizontal: 20 }}
        onPress={() => navigation.openDrawer()}
      >
        <Icon name="menu" color={"#000"} size={20} />
      </TouchableOpacity>
    )
  });




  render() {
    displayNotification = () => {
      if (!this.props.connection) {
        <View
          style={{
            backgroundColor: "red",
            color: '#ffffff',
            alignItems: "center",
            fontFamily: 'OpenSans-Semibold',
            justifyContent: "center",
            fontSize:14,
            textAlign: "center"
          }}
        >
          <Text>Your Offline, :(</Text>
        </View>;
      }




      if (this.props.loading) {
        return (
          <View
            style={{
              backgroundColor: "#598BFF",
              color: "#ffffff",
              alignItems: "center",
              fontFamily: 'OpenSans-Semibold',
              justifyContent: "center",
              fontSize:14,
              textAlign: "center"
            }}
          >
            <Text>Loading questions...</Text>
          </View>
        );
      }
    
      
    };

    
    if (this.props.loading && !this.props.questions) {
      return (
        <Container backgroundColor="#F5F5F5">
          <ActivityIndicator size="large" color="#0000ff" />
        </Container>
      );
    }
    return (
      <ContainerBackground backgroundColor="#F5F5F5">
    
        <FlatList
          data={this.props.questions}
          renderItem={question => {
            return (
              <View
                style={{
                  flex: 1,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Feeds question={question} />
              </View>
            );
          }}
          onEndReachedThreshold={0.5}
          onEndReached={() => this.props.getQuestion()}
          keyExtractor={question => question.id}
          ListFooterComponent={
            this.props.loadingmore ? (
              <View>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            ) : (
              <View />
            )
          }
        />
        <TouchableOpacity
          style={{
            borderWidth: 1,
            borderColor: "#598BFF",
            alignItems: "center",
            justifyContent: "center",
            width: 70,
            position: "absolute",
            bottom: 10,
            right: 10,
            height: 70,
            backgroundColor: "#598BFF",
            borderRadius: 100
          }}
          onPress={() => {
            this.props.navigation.navigate("Question");
          }}
        >
          <Icon name="pencil" color={"#fff"} size={24} />
        </TouchableOpacity>
      </ContainerBackground>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getQuestion: () => dispatch(actions.getQuestion())
});
const mapStateToProps = state => {
  return {
    questions: state.questionReducer.questions,
    loading: state.questionReducer.loading,
    loadingmore: state.questionReducer.loadingmore,
    connection:state.network.isConnected
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
