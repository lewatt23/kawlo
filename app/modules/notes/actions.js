import * as t from './actionTypes';
import * as api from './api';
import { takeLatest, put, call, select } from 'redux-saga/effects';



export  const getnotes =(url) => {
     
  return {
      type:t.GET_NOTES_START,
      url:url,
  };


}




//sagas


//funtion to  login user and get  his data
function* LoadNotes(action) {
  try {
   
          yield put({ type: t.GET_NOTES_FAILED, payload: {loading:true} });

         const result = yield call(api.getNotes,action.url);
         
       

         yield put({ type: t.GET_NOTES_SUCCESS, payload: {note:result ,loading:false} });

         
     } catch (error) {
         console.log('saga fail: ', error);
         yield put({ type: t.GET_NOTES_FAILED, payload: {loading:false} });
     }
 }


 
export function* watcherGetnotes(action) {
    
  yield takeLatest(t.GET_NOTES_START, LoadNotes);
}
