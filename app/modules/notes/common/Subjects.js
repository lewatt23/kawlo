import React from "react";
import {
 
  View,
  TouchableOpacity,
  FlatList
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { withNavigation } from "react-navigation";
import ListNotes from "../common/ListNotes";
import { connect } from "react-redux";
import * as actions from "../actions";
import {ContainerBackground }from './../../../components/atoms/container/container';



//called note list   in route

class Subjects extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <TouchableOpacity
        style={{ marginHorizontal: 20 }}
        onPress={() => navigation.openDrawer()}
      >
        <Icon name="menu" color={"#000"} size={20} />
      </TouchableOpacity>
    )
  });

  componentWillMount() {
    if (this.props.connection) {
      console.log("connection states",this.props.connection);
      this.props.getnotes(
        "http://cameroongcerevision.com/wp-json/wp/v2/posts?_embed&&categories=1"
      );
    }
  }

  render() {

    if (this.props.loading && !this.props.note) {
      return (
        <Container backgroundColor="#F5F5F5">
          <ActivityIndicator size="large" color="#0000ff" />
        </Container>
      );
    }
    return (
      <ContainerBackground backgroundColor="#F5F5F5">
        <FlatList
          data={this.props.note.data}
          renderItem={note => {
            return (
              <View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Article", {
                      Article: note
                    })
                  }
                >
                  <ListNotes notes={note} />
                </TouchableOpacity>
              </View>
            );
          }}
          onEndReachedThreshold={0.5}
          keyExtractor={note => note.id}
         
        />
      </ContainerBackground >
    );
  }
}



const mapDispatchToProps = dispatch => ({
  getnotes: url => dispatch(actions.getnotes(url))
});
const mapStateToProps = state => {
  return {
    note: state.notesReducer.note,
    loading: state.notesReducer.loading,
    connection: state.network.isConnected
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Subjects)
);
