import React from 'react';
import { StyleSheet, Text, View ,Image ,Dimensions } from 'react-native';
import { withNavigation } from 'react-navigation'; 
import HTML from 'react-native-render-html';
import {CardNotes} from './../../../components/molecules/card/card';




 class ListNotes extends React.Component {
 
 
 
componentDidMount(){
   

}



  render() {
    const {notes} = this.props
    //preparing data
    const title = !!notes.item?notes.item.title.rendered:'';
    const image = !!notes.item?notes.item.image:'';
    const content = <HTML
      html={notes.item.excerpt.rendered}
      imagesMaxWidth={Dimensions.get('window').width}
    />;
 
   



    return (
      
      

   <View>
          <CardNotes
          title={title}
          image={image}
          text={content}
          cardAction={()=> this.props.navigation.navigate("Article",{ notes: this.props.notes  })}
          />
          
    </View>
          
   
       

    );
  }
}

const styles = StyleSheet.create({
  container: {
      backgroundColor: '#fff',
    },
 
});

export default withNavigation(ListNotes);