import React from "react";
import { View, ScrollView, Dimensions } from "react-native";
import { withNavigation } from "react-navigation";
import HTML from "react-native-render-html";
import styled from "styled-components";
import Title from "./../../../components/atoms/typography/typography";
import {
  CardImage,
  CardTextNotes
} from "./../../../components/molecules/card/card";

class Article extends React.Component {
  state = {
    notes: this.props.navigation.getParam("notes", null)
  };

  componentDidMount() {}

  render() {
    const { notes } = this.state;
    //preparing data
    const title = !!notes.item ? notes.item.title.rendered : "";
    const image = !!notes.item ? notes.item.image : "";
    const content = (
      <HTML
        html={notes.item.content.rendered}
        imagesMaxWidth={Dimensions.get("window").width}
      />
    );

    return (
      <View style={{ backgroundColor: "#fff", height: "100%" }}>
        <ScrollView>
          <View style={{ padding: 10 }}>
            <Title text={title} textColor="#000" />

            {!!image && (
              <CardImageContainer
                onPress={() =>
                  this.props.navigation.navigate("ImageView", {
                    image: this.state.question.item.image
                  })
                }
              >
                <CardImage source={{ uri: image }} />
              </CardImageContainer>
            )}

            <CardTextNotes>{content}</CardTextNotes>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const CardImageContainer = styled.TouchableOpacity``;

export default withNavigation(Article);
