import * as t from './actionTypes';

let initialState = { isLoggedIn: false,note:[],loading:false };

const notesReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.GET_NOTES_START:
      state = Object.assign({}, state, initialState);
      return state;

    case t.GET_NOTES_SUCCESS:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.GET_NOTES_FAILED:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    

    default:
      return state;
  }
};

export default notesReducer;
