import React from 'react';
import { StyleSheet, Text, View,ImageBackground  } from 'react-native';
import Swiper from 'react-native-swiper';


/**
 * Splash  screens.
 * @return {view} Splash screens.
 */
export class Splash extends React.Component {
  render() {
    return (
      <Swiper 
         nextButton={<Text style={{color:'white',fontSize:45}}>›</Text>}
         prevButton={<Text style={{color:'white',fontSize:45}}>‹</Text>}
         buttonColor={'white'}
         activeDotColor={'white'}
         style={styles.wrapper}
         showsButtons={true}
         loop={false}
         >
        
          
          <ImageBackground source={require('../../../../assets/welcome-1.png')} style={{width: '100%', height: '100%'}}>
        
          </ImageBackground>
          
          <ImageBackground source={require('../../../../assets/welcome-2.png')} style={{width: '100%', height: '100%'}}>
          </ImageBackground>
          
          <ImageBackground source={require('../../../../assets/welcome-3.png')} style={{width: '100%', height: '100%'}}>
        
          </ImageBackground>
          
       
    

      </Swiper>

    );
  }
}


const styles = StyleSheet.create({
  wrapper: {
  },
  
})