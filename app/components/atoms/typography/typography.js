import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Title = props => (
    <BoldContainer>
		<TextTitle textColor={props.textColor}>{props.text}</TextTitle>
    </BoldContainer>
);

const TitleSub = props => (
    <BoldContainer>
    <TextSub textColor={props.textColor}>{props.text}</TextSub>
    </BoldContainer>
);

const  TextSubNormal = props => (
    <BoldContainer>
    < TextSubNormalText textColor={props.textColor}>{props.text}</TextSubNormalText>
    </BoldContainer>
);
const BlodLink = props => (
	<BoldContainer
        onPress={props.boldAction}
    >
		<TextBold bold={props.bold} padding={props.padding} textColor={props.textColor}>{props.text}</TextBold>
    </BoldContainer>
);



const TextTitle = styled.Text`
    font-size: 18px;
	color: ${props => props.textColor};
	font-family:OpenSans-Semibold;
	text-align: left;
`;
const TextSub = styled.Text`
    font-size: 14px;
	color: ${props => props.textColor};
	font-family:OpenSans-Italic;
	text-align: left;
`;
const TextSubNormalText = styled.Text`
    font-size: 14px;
	color: ${props => props.textColor};
	font-family:OpenSans-Regular;
	text-align: left;
`;
const TextBold = styled.Text`
    font-size: 14px;
	color: ${props => props.textColor};
	font-family:OpenSans-Italic;
    text-align: left;
    font-weight:${props => props.bold?'bold':'normal'}
    padding:${props => props.padding}
`;

const BoldContainer= styled.TouchableOpacity`
	width: 90%;
    text-align: left;
   
`;


export default Title;
export {TitleSub, BlodLink, TextSubNormal};


Title.propTypes = {
    TextColor : PropTypes.string,
	Text: PropTypes.string,
	
}  
TitleSub.propTypes = {
    TextColor : PropTypes.string,
	Text: PropTypes.string,
	
}  
BlodLink.propTypes = {
    TextColor : PropTypes.string,
    Text: PropTypes.string,
    boldAction:PropTypes.func,
    bold:PropTypes.bool,
    padding:PropTypes.string

	
}  