import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.View`
    flex: 1;
    background-color: ${props => props.backgroundColor};
    justify-content: center;
    align-items: center;
`;
const ContainerBackground  = styled.View`
    flex: 1;
    background-color: ${props => props.backgroundColor};

`;
const  Columns = styled.View`
 
    flex-direction:column;
    justify-content: space-around; 
    align-items: center;

`;

const Row = styled.View`
  
    flex-direction: row;
    justify-content:space-around; 
    align-items: center;
    
`;


export default Container;
export {ContainerBackground,Row,Columns};


Container.propTypes = {
   backgroundColor: PropTypes.string,
}  