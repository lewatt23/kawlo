import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const Button = props => (
	<ButtonContainer
        onPress={props.buttonAction}
        backgroundColor={props.buttonBackgroundColor}
	>
		<ButtonText textColor={props.buttonTextColor}>{props.buttonText}</ButtonText>
	</ButtonContainer>
);

const ButtonContainer = styled.TouchableOpacity`
	width: 90%;
    text-align: center;
	padding: 7px;
	border-radius: 7px;	
	background-color: ${props => props.backgroundColor};
`;

const ButtonText = styled.Text`
    font-size: 18px;
	color: ${props => props.textColor};
	font-family:OpenSans-Semibold;
	text-align: center;
`;




export default Button;


Button.propTypes = {
    buttonAction: PropTypes.func,
	buttonBackgroundColor: PropTypes.string,
	buttonTextColor : PropTypes.string,
	buttonText: PropTypes.string,
	
}  