import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const Loader = props => (
	<LoaderContainer>
		<LoaderIndicator animating={props.loaderAnimating} color={props.loaderColor} size={props.loaderSize} />
	</LoaderContainer>
);

const LoaderContainer = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center
`;

const LoaderIndicator  = styled.ActivityIndicator`
    font-size: 18px;
	font-family:OpenSans-Semibold;
	text-align: center;
`;




export default Loader;


Loader.propTypes = {
   loaderSize: PropTypes.string,
   loaderColor: PropTypes.string,
   loaderAnimating: PropTypes.string
}  