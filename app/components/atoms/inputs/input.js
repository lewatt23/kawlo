import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Input = props => (
	<InputContainer>

        <InputText
         multiline={props.multiline?props.multiline:false}
         numberOfLines={20}
         placeholderTextColor={props.placeholderTextColor}
         placeholder={props.placeholder}
         onPress={props.inputAction}
         backgroundColor={props.backgroundColor}
         underlineColorAndroid={props.underlineColorAndroid}
         editable={props.editable}
         autoCorrect={props.autoCorrect}
         value={props.value}
         onChangeText={props.onChangeText}
         autoFocus={props.autoFocus}
         textColor={props.textColor}
         borderColor={props.borderColor}
         secureTextEntry={props.secureTextEntry}
         />
	</InputContainer>
);


const InputTextArea = props => (
	<InputContainer>

        <InputAreaText
         multiline={true}
         numberOfLines={props.numberOfLines}
         placeholderTextColor={props.placeholderTextColor}
         placeholder={props.placeholder}
         onPress={props.inputAction}
         backgroundColor={props.backgroundColor}
         underlineColorAndroid={props.underlineColorAndroid}
         editable={props.editable}
         autoCorrect={props.autoCorrect}
         value={props.value}
         onChangeText={props.onChangeText}
         autoFocus={props.autoFocus}
         textColor={props.textColor}
         borderColor={props.borderColor}
         secureTextEntry={props.secureTextEntry}
         />
	</InputContainer>
);







const InputContainer = styled.View`
	width: 90%;
    text-align: left;
	padding: 7px;
`;

const InputText = styled.TextInput`
    font-size: 16px;
    color:${props => props.textColor};
    padding-left:5px;
    height:38px;
    width: 100%;
    font-family:OpenSans-Regular;
    border-radius: 7px;	
    border-width:1px;
    border-color: ${props => props.borderColor};
    text-align: left;
    background-color:${props => props.backgroundColor};
`;


const InputAreaText = styled.TextInput`
    font-size: 16px;
    color:${props => props.textColor};
    padding-left:5px;
    
    width: 100%;
    font-family:OpenSans-Regular;
    border-radius: 7px;	
    border-width:1px;
    border-color: ${props => props.borderColor};
    text-align: left;
    background-color:${props => props.backgroundColor};
`;




export default Input;
export {InputTextArea };

Input.propTypes = {
    onChangeText: PropTypes.func,
	backgroundColor: PropTypes.string,
	underlineColorAndroid : PropTypes.string,
	editable : PropTypes.bool,
	autoCorrect : PropTypes.bool,
    placeholder: PropTypes.string,
    multiline:PropTypes.bool,
	placeholderTextColor: PropTypes.string,
	textColor: PropTypes.string,
	borderColor: PropTypes.string,
    value: PropTypes.string,
    numberOfLines:PropTypes.number,
	autoFocus: PropTypes.bool
	
}  