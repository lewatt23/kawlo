import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Title from "./../../atoms/typography/typography";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {truncate} from '../../../utils/smallfunctions';


const Card = props => (
  <CardContainer>
    <CardBody onPress={props.cardAction}>
      <Title text={props.title} textColor="#000" />
      {!!props.image && <CardImage source={{ uri: props.image }} />}

      <CardText>{truncate(props.text)}</CardText>
    </CardBody>

    <CardButtom>
      <Row>
        <Columns>
          <Row>
            <Columns>
              <ImageCard source={{ uri: props.user_avater }} />
            </Columns>
            <Columns>
              <TextCard fontSize={"12px"}>{"By " + props.user_name}</TextCard>
              <TextItalicCard>{props.time}</TextItalicCard>
            </Columns>
          </Row>
        </Columns>

        <Columns>
          <Badge level={props.cat}>
            <BadgeText>{props.cat + "-" + props.subject}</BadgeText>
          </Badge>
        </Columns>

        <Columns>
          <Row>
            <Columns>
           
              <Icon name="heart" color={"red"} size={25} />
            </Columns>
            <Columns>
           
              <TextCard>{props.likes}</TextCard>
         
            </Columns>
          </Row>
        </Columns>

        <Columns>
          <Row>
            <Columns>
              <Icon name="message-text-outline" color={"#75a5ee"} size={25} />
            </Columns>
            <Columns>
              <TextCard>{props.reply}</TextCard>
            </Columns>
          </Row>
        </Columns>
      </Row>
    </CardButtom>
  </CardContainer>
);

export const CardNotes = props => (
  <CardContainer>
    <CardBody onPress={props.cardAction}>
      <Title text={props.title} textColor="#000" />
      {!!props.image && <CardImage source={{ uri: props.image }} />}

      <CardTextNotes>{truncate(props.text)}</CardTextNotes>
    </CardBody>

  </CardContainer>
);







export const CardContainer = styled.View`
	width: 98%;
    text-align: left;
    padding: 10px;
    shadow-color: #000;
    shadow-offset: {
      width: 0,
      height: 1,
    };
    shadow-opacity: 0.20;
    shadow-radius: 1.41;
    
    elevation: 2;


    margin-top:10px;
    border-radius:7px;
    border-width:1px;
    border-color:#fff;
    background-color : #fff;
    justify-content: center;

    
`;

export const CardBody = styled.TouchableOpacity``;

export const CardText = styled.Text`
  text-align: left;
  margin-bottom: 4px;
  margin-top: 4px;
  color: #969696;
  font-size: 14px;
`;
export const CardTextNotes = styled.View`
  text-align: left;
  margin-bottom: 4px;
  margin-top: 4px;
  color: #969696;
  font-size: 18px;
`;


 const Row = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;
export const CardButtom = styled.View`
  padding-top: 10px;
  padding-bottom: 5px;
`;

 const Columns = styled.View`
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const CardImage = styled.Image`
  width: 100%;
  height: 150px;
  border-radius: 7;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 5px;
`;
export const ImageCard = styled.Image`
  width: 30;
  height: 30;
  border-radius: 20;
`;

export const TextCard = styled.Text`
  font-family: OpenSans-Regular;
  font-size: 10px;
  padding-left: 4px;
  text-align: left;
`;
export const TextItalicCard = styled.Text`
  font-family: OpenSans-Italic;
  font-size: 10px;
  padding-left: 4px;
`;

export const Badge = styled.TouchableOpacity`
  width: 100%;
  text-align: center;
  padding: 7px;
  border-radius: 7;
  background-color: ${props =>
    props.level === "O level" ? "#AAD23A" : "#EF7231"};
`;

export const BadgeText = styled.Text`
  font-size: 10px;
  color: #fff;
  font-family: OpenSans-Semibold;
  text-align: center;
`;

const WrapperCard = styled.View``;

const WrapperButton = styled.TouchableOpacity``;

export default Card;

Card.propTypes = {};
