import { NavigationActions ,Navigation} from 'react-navigation';
import { Provider } from 'react-redux';
import {Alert} from 'react-native'
import {Home} from '../../modules/home';
import DropdownAlert from 'react-native-dropdownalert';


let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;

}







function navigate(routeName, params) {
  if (_navigator) {
    Alert.alert('ERROR: Attempted to navigate before initialization: ' + routeName)
  }


 
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

export default {
  navigate,
  setTopLevelNavigator,
};