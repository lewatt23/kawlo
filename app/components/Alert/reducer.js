import * as t from './actionTypes';

let initialState = {
    alert : {
      state:false,
      type:null,
      title:null,
      message:null
  },
    
};

const alertReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.CALL_ALERT:
      state = Object.assign({}, state, { ...action.payload });
      return state;

    case t.RESET_ALERT:
    state = Object.assign({}, state, { ...action.payload });
      return state;
   
   default:
      return state;
  }
};

export default alertReducer;
