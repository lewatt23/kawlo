import { StyleSheet } from 'react-native';
import { scaleVertical } from '../utils/scale';


export const UtilStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  titleText: {
    fontSize: 20,
    fontFamily: 'Roboto-Regular',

    
  },
  titleFeeds: {
    fontFamily:"OpenSans-Regular",
    fontSize: 14,
    paddingHorizontal:10,
    fontWeight: 'bold',
    flex:1,
    textAlign:'center'

    
  },
  loadWidth:{
   width:'100%'
  },
  section: {
    marginTop: 14,
    paddingHorizontal: 24,
    paddingVertical: 15,
  },
  bordered: {
    borderBottomColor: '#0000001A',
    borderBottomWidth: 0.5,
  },
  rowContainer: {
    marginTop: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  columnContainer: {
    marginTop: 16,
  },
  spaceAround: {
    marginHorizontal: 5,
  },
  feeds:{
    padding: 15,
  },
  heading:{
    fontFamily:"OpenSans-Regular",
    
    fontSize: 16,
  },
  headingtitle:{
    fontFamily:"OpenSans-Regular",

    fontSize: 18,
  },
  spaceH: {
    marginHorizontal: 5,
  },
  spaceTop: {
    marginTop: 8,
  },
  spaceBottom: {
    marginBottom: 8,
  },
  spaceVertical: {
    marginVertical: 8,
  },
  camera:{
    justifyContent:"center",
    alignItems: 'center',
  },
  containertwo: {
    alignItems: 'center',
    marginVertical: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
  
    borderWidth:1,
    borderRadius:15,
    borderColor:"#d3d3d3",
    padding: 20,
    position: 'absolute',
    bottom:0,
    left:0,
    right:0,

},
    IconInput:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff',
    },
  containerthree: {
    alignItems: 'center',
    marginVertical: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
    
    padding: 20,
    
},
line:{
  height:0.5,
  backgroundColor:"#ebebeb"
},
  dropdown:{
    height:50,
    width:300,
    borderColor: "#d3d3d3",
    borderWidth: 1,
  },
  feedImage:{
    width:300,
    height:150,
    borderRadius:15,
    marginVertical: 8,
  },
  feedsBottom:{
    paddingTop:10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  feedsBottomIcons:{
    alignItems: 'center',
    justifyContent:'center'
  },
  
   feedsBottomIconsText:{
    fontSize: 11,
    color:"#3c3c3c"
  },
  
  feedsBottomIconsBottom:{
    paddingLeft: 10,
 },
  validate:{
    color:'red',
    marginVertical: 5,
  },
  validatem:{
    color:'red',
    marginVertical: 2,
  },
   feedsBottomIconsTextReply:{
    fontSize: 11,
    color:"#3c3c3c",
    marginLeft: 8,


  },
  description: {
    flexDirection: 'row',
    paddingTop:10,
    paddingBottom:10,
    marginTop:5,
    marginBottom: 5,
  },
  avater:{
    width:30,
    height:30,
    borderRadius:15
  },
  author:{
    fontSize:12,
    paddingTop:5,
    color:"#000"
  },
  floatButton:{
    borderWidth:1,
    borderColor:'#75a5ee',
    alignItems:'center',
    justifyContent:'center',
    width:70,
    position: 'absolute',                                          
    bottom: 10,                                                    
    right: 10,
    height:70,
    backgroundColor:'#75a5ee',
    borderRadius:100,
  },
  link:{
    color:'#75a5ee',
    fontFamily: 'OpenSans-Regular',
  },
  textInput:{
    height:scaleVertical(56),
    paddingLeft: 6,
    color:'#757575',
    borderColor: "#d3d3d3",
    borderRadius: 28,
    fontFamily: 'OpenSans-Regular',
    borderWidth: 1,
    width:300,
    marginTop:5,
    marginBottom:5,
  },
  textInputModal:{
    height:scaleVertical(56),
    marginLeft: 10,
    paddingLeft:6,
    color:'#757575',
    borderColor: "#d3d3d3",
    borderRadius: 28,
    fontFamily: 'OpenSans-Regular',
    borderWidth: 1,
    width:250,
    marginTop:5,
    marginBottom:5,
  },
  textArea:{
    flex: 1,
    height:scaleVertical(102),
    paddingHorizontal: 15,
    paddingVertical: 10,
    color:'#757575',
    borderColor: "#d3d3d3",
    borderRadius: 28,
    fontFamily: 'OpenSans-Regular',
    borderWidth: 1,
    width:300,
   
    
  },
  exampleView: {
    paddingRight: 10,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flex: 1,
  },
  text: {
    color: '#000',
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
  },
  codeText: {
    color: '#000',
  },
  tab: {
    paddingLeft: 20,
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    paddingTop: 20,
  },
  column: {
    flexDirection: 'column',
  },
  tabContent: {
    fontSize: 32,
    alignSelf: 'center',
    padding: 30,
    color: '#000',
  },
  header__drawer:{
    backgroundColor: "#5d95d9",
    height:150,
  },
  header__setting:{
    backgroundColor: "#5d95d9",
    height:150,
  },
  avatar__drawer: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:150,
    alignSelf:'center',
    position: 'absolute',
    marginTop:100
  },
  avatar__setting: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    alignSelf:'center',
    marginTop:10,
    position: 'absolute',
  },
  name__drawer:{
    fontSize:18,
    color: "#696969",
    fontWeight: "300"
  },
  
  name__setting:{
    fontSize:18,
    color: "#fff",
    fontWeight: "300",
    position: 'absolute',
    alignSelf:'center',
    marginTop:120
    
  },
 
  body__drawer:{
    marginTop:60,
  },
 
  
 
});

export const buttons = {
  base: {
    button: {
      alignItems: 'stretch',
      paddingVertical: 0,
      paddingHorizontal: 0,
      height: scaleVertical(40),
      borderRadius: 20,
      backgroundColor: 'transparent',
      textAlign: 'center',
    },
    gradient: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      colors: '#5d95d9',
      backgroundColor:'#5d95d9'
    },
    text: {
      backgroundColor: 'transparent',
      color: '#fff',
      fontSize: 20,
      fontFamily: 'OpenSans-Regular',
      textAlign: 'center',
      paddingTop: 15,

    },
  },
  large: {
    button: {
      alignSelf: 'stretch',
      height: scaleVertical(56),
      borderRadius: 28,
      
    },
    gradient: {
      borderRadius: 28,
    },
  },
  statItem: {
    button: {
      flex: 1,
      borderRadius: 5,
      marginHorizontal: 10,
      height: null,
      alignSelf: 'auto',
    },
    gradient: {
      flex: 1,
      borderRadius: 5,
      padding: 10,
    },
  },
}