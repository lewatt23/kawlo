import React from 'react';
import PropTypes from 'prop-types';
import { 
  Alert
}
from 'react-native';

//to  checks later
export class AlertKawlo extends React.Component {

    render() {

  

        return(
            Alert.alert(
                'Error',
                this.props.message,
                { cancelable: false }
               
              )
        )

    }

}


//props type  checking  for message
AlertKawlo.propTypes = {
    message: PropTypes.string,

  }
  
// default  type  for message
  AlertKawlo.defaultProps = {
    message: 'message',
   }
