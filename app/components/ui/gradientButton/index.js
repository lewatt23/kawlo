import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import {
  Text
} from 'react-native';
import {UtilStyles,buttons } from '../../styles';


export class GradientButton extends React.Component  {
   

  renderContent = (textStyle) => {
    const hasText = this.props.text === undefined;
    return hasText ? this.props.children : this.renderText(buttons.base.text);
  };

  renderText = (textStyle) => (
    <Text style={textStyle}>{this.props.text}</Text>
  );

  render() {

   
   const  buttonGradient =buttons.large.button; 
    return (
      
        <LinearGradient
          colors={['#75a5ee','#5d95d9','#4581c5']}
          start={{ x: 0.0, y: 0.5 }}
          end={{ x: 1, y: 0.5 }}
          style={buttonGradient}
          >
         
          {this.renderContent()}
        </LinearGradient>
        
      
    );
  }
}